OpenCV2-Python-Guide
====================

このレポジトリはOpenCV2−Python Tutorials **https://opencv-python-tutroals.readthedocs.org/en/latest/index.html** を和訳したものです．

2016/06/11に一通り翻訳しましたが，直訳や誤訳を見つけたら是非イシューとして報告してください．

データファイル
-----------

このチュートリアルで使用する入力データは **data**ディレクトリに保存されています．

オンライン
---------

* **公式チュートリアルは, 以下を参照してください : http://docs.opencv.org/3.1.0/d6/d00/tutorial_py_root.html#gsc.tab=0**
* http://labs.eecs.tottori-u.ac.jp/sd/Member/oyamada/OpenCV/html/index.html - このページは確認用のページなので，エラーが含まれている可能性があります．公式チュートリアルを参照してください．

オフライン
---------
このドキュメントをソースからビルドするには
* sphinxをインストールする
* このレポジトリをダウンロードもしくはクローン作成し，rootディレクトリに移動する
* コマンドを実行する : `make html` ，そうすると， **build/html/** ディレクトリにhtmlファイルが作成される
