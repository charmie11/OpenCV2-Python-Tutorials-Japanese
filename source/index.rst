﻿.. OpenCV-Python Tutorials documentation master file, created by
   sphinx-quickstart on Fri May 31 12:04:12 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

OpenCV-Python チュートリアル文書のページへようこそ!
===================================================

このチュートリアルは `OpenCV-Python-Tutorials <https://opencv-python-tutroals.readthedocs.org/en/latest/>`_ を和訳したものです．

現在(2016/03/08)，py_feature2d(特徴点検出，特徴量記述子)に関するチュートリアルの翻訳が終わっていません．

内容:

.. toctree::
   :maxdepth: 2
   
   py_tutorials/py_tutorials

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

