.. _PY_Table-Of-Content-Video:

動画解析
------------------------------------------

*  :ref:`meanshift`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |vdo_1|     既に色に基づく追跡を扱いましたが，簡単でしたよね．今回は "Meanshift" という，物体追跡のためのより良いアルゴリズムと，Meanshiftの改良版である "Camshift" を扱います．

  =========== ======================================================

  .. |vdo_1|  image:: images/camshift.jpg
                 :height: 90pt
                 :width:  90pt


*  :ref:`Lucas_Kanade`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |vdo_2|     ここでは動画解析などのアプリケーションにおける重要な概念である "Optical Flow" について学びましょう．
  =========== ======================================================

  .. |vdo_2|  image:: images/opticalflow.jpeg
                 :height: 90pt
                 :width:  90pt                 


*  :ref:`background_subtraction`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |vdo_b|     物体追跡のようなアプリケーションでは，前景物体を抽出する必要が有ります．背景差分はこのような時によく使われる手法です．
  =========== ======================================================

  .. |vdo_b|  image:: images/background.jpg
                 :height: 90pt
                 :width:  90pt  


                 
.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   ../py_meanshift/py_meanshift
   ../py_lucas_kanade/py_lucas_kanade
   ../py_bg_subtraction/py_bg_subtraction
