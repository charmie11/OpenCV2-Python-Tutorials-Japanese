﻿.. _PY_Table-Of-Content-Calib:


カメラキャリブレーションと3次元復元
----------------------------------------------

*  :ref:`calibration`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |calib_1|   カメラの良さを理解しましょう．撮影した画像中に何かしらの歪みはありますか？もしあるとすれば，どうやって修正すれば良いでしょうか？

  =========== ======================================================

  .. |calib_1|  image:: images/calibration_icon.jpg
                 :height: 90pt
                 :width:  90pt


*  :ref:`pose_estimation`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |calib_2|   このチュートリアルではキャリブレーションモジュールを使ったかっこいい3次元効果を作成する方法を少しだけ紹介します．

  =========== ======================================================

  .. |calib_2|  image:: images/pose_icon.jpg
                 :height: 90pt
                 :width:  90pt


*  :ref:`epipolar_geometry`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |calib_3|   エピポーラ幾何とエピポーラ高速について理解しましょう．

  =========== ======================================================

  .. |calib_3|  image:: images/epipolar_icon.jpg
                 :height: 90pt
                 :width:  90pt
                 

*  :ref:`py_depthmap`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |calib_4|   複数枚の2次元画像から距離情報を抽出しましょう．

  =========== ======================================================

  .. |calib_4|  image:: images/depthmap_icon.jpg
                 :height: 90pt
                 :width:  90pt
              
                 
               
.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   ../py_calibration/py_calibration
   ../py_pose/py_pose
   ../py_epipolar_geometry/py_epipolar_geometry
   ../py_depthmap/py_depthmap
      
