﻿.. _PY_Table-Of-Content-Gui:

OpenCVのGUI機能
-----------------------------------------------------------


*  :ref:`Display_Image`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |gui_1|     画像の読み込み，表示，保存方法を学びます．

  =========== ======================================================

  .. |gui_1|  image:: images/image_display.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Display_Video`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |gui_2|     動画の再生方法，カメラを使った撮影方法と動画ファイルとしての保存方法を学びます．

  =========== ======================================================

  .. |gui_2|  image:: images/video_display.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Drawing_Functions`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |gui_5|     OpenCVを使った基本形状オブジェクトの描き方を学びます．

  =========== ======================================================

  .. |gui_5| image:: images/drawing.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Mouse_Handling`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |gui_3|     マウスを使った描画機能を学びます．

  =========== ======================================================

  .. |gui_3| image:: images/mouse_drawing.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Trackbar`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |gui_4|     パラメータ調整のためのトラックバーの作り方を学びます．

  =========== ======================================================

  .. |gui_4| image:: images/trackbar.jpg
                 :height: 90pt
                 :width:  90pt
                 
.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   ../py_image_display/py_image_display
   ../py_video_display/py_video_display
   ../py_drawing_functions/py_drawing_functions
   ../py_mouse_handling/py_mouse_handling
   ../py_trackbar/py_trackbar
