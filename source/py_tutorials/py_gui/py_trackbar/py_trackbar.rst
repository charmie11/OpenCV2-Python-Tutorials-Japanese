﻿.. _Trackbar:

カラーパレットとしてのトラックバー
*****************************************

目的
=====

.. container:: enumeratevisibleitemswithsquare

    * OpenCVのウィンドウにトラックバーをつける方法を学びます．
    * 以下の関数の使い方を学びます : **cv2.getTrackbarPos()**, **cv2.createTrackbar()** etc.
    
デモ
==========

このチュートリアルは，トラックバーの位置によって指定した色を表示する簡単なプログラムを作成します．ウィンドウには3個のトラックバーが表示されており，トラックバーはそれぞれ青，緑，赤色の数値の指定に使います．トラックバーの位置に応じてウィンドウに表示される色が変わります．デフォルトの色は黒に設定されています．

**cv2.getTrackbarPos()** 関数の第1引数はトラックバーの名前，第2引数は第1引数で指定したトラックバーが表示されているウィンドウの名前，第3引数はトラックバーのデフォルト値，第4引数はトラックバーの取りうる最大値，第5引数はトラックバーの位置が変わる度に呼び出されるコールバック関数です．このコールバック関数は常にトラックバーの現在位置をデフォルト引数として受け取ります．今回のデモコードのコールバック関数は何の処理もしないのでただ関数に引数として渡すことしかしません．

トラックバーのもう一つの重要な使い方として，ボタンや切り替え機能としての使い方が挙げられます．OpenCVはデフォルトでボタンを実装するような関数を用意していませんが，トラックバーをボタンや切り替え機能として使うことはできます．このチュートリアルで実装するアプリケーションでは，スイッチがOFF時にはウィンドウを真っ暗にし，ON時にのみ色を表示するスイッチを作成します．
::

    import cv2
    import numpy as np

    def nothing(x):
        pass

    # Create a black image, a window 
    img = np.zeros((300,512,3), np.uint8)
    cv2.namedWindow('image')
    
    # create trackbars for color change
    cv2.createTrackbar('R','image',0,255,nothing)
    cv2.createTrackbar('G','image',0,255,nothing)
    cv2.createTrackbar('B','image',0,255,nothing)

    # create switch for ON/OFF functionality
    switch = '0 : OFF \n1 : ON'
    cv2.createTrackbar(switch, 'image',0,1,nothing)
    
    while(1):
        cv2.imshow('image',img)
        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break
        
        # get current positions of four trackbars
        r = cv2.getTrackbarPos('R','image')
        g = cv2.getTrackbarPos('G','image')
        b = cv2.getTrackbarPos('B','image')
        s = cv2.getTrackbarPos(switch,'image')
        
        if s == 0:
            img[:] = 0
        else:
            img[:] = [b,g,r]
        
    cv2.destroyAllWindows()

このコードを実行した結果のスクリーンショットはいかのようになります． :

     .. image:: images/trackbar_screenshot.jpg
              :alt: Screenshot of Image with Trackbars
              :align: center 
              
課題
===========

#. トラックバーを使って，色とブラシのサイズ(半径)を調整可能なペイントアプリケーションを作成しなさい．描画機能についてはマウス使用に関する前チュートリアルを参照してください．
