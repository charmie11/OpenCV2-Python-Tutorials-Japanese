﻿.. _Drawing_Functions:

OpenCVの描画機能
******************************

目的
=====

.. container:: enumeratevisibleitemswithsquare

    * OpenCVを使って色々な幾何学的形状を持つオブジェクトを描画する方法を学びます
    * 以下の関数の使い方を学びます: **cv2.line()**, **cv2.circle()** , **cv2.rectangle()**, **cv2.ellipse()**, **cv2.putText()** etc.
    
コード
==========

上記全ての関数に共通する引数を以下に示します．:
    
    * img : オブジェクトを描画する画像
    * color : オブジェクトの色．BGRで指定する場合はtupleとして指定する，例えば青であれば ``(255,0,0)`` ．グレースケールで指定する場合は単にスカラー値を指定する．
    * thickness : 線や円などの太さ． 閉じている図形(例えば円)に対して **-1** が指定された場合，そのオブジェクトは塗りつぶされる． *デフォルト値は1*
    * lineType : 線のタイプ， 8連結，アンチーエイリアス等．*デフォルトは8連結．* ``cv2.LINE_AA`` を指定すると曲線の描画に適したアンチーエイリアスになる．

直線の描画
-------------
直線を描画するには直線の始点と終点の座標を指定する必要があります．以下のコードは真っ黒な画像を生成し，画像の左上から右下に向かって青い線を描画します．
::

    import numpy as np
    import cv2
    
    # Create a black image
    img = np.zeros((512,512,3), np.uint8)
    
    # Draw a diagonal blue line with thickness of 5 px
    img = cv2.line(img,(0,0),(511,511),(255,0,0),5)

長方形の描画
-------------------
長方形を描画するには長方形の左上と右下の角の座標を指定する必要があります．以下のコードは画像の右上に緑色の長方形を描画します．
::
    
    img = cv2.rectangle(img,(384,0),(510,128),(0,255,0),3)
    
円の描画
----------------
円を描画するには中心の座標と半径を指定します．以下のコードは，上記のコードで描画した四角形の内側に円を描画します．
::

    img = cv2.circle(img,(447,63), 63, (0,0,255), -1)
    
楕円の描画
--------------------

楕円を描画するためには幾つかの引数を指定する必要があります．第1引数は楕円の中心座標(x,y)，第2引数は軸の長さ(長径, 短径)，第3引数 ``angle`` は楕円の偏角を反時計回りで指定します．``startAngle`` と ``endAngle`` は楕円を描画する始角と終角を長径から時計回りの方向で指定します(例えば0 と 360 を指定すると完璧な楕円が描ける)．詳細については **cv2.ellipse()** の説明を参照してください．以下のコードは画像の中心に半楕円を描画します．
::

    img = cv2.ellipse(img,(256,256),(100,50),0,0,180,255,-1) 


多角形の描画
------------------
多角形を描画するためには多角形を形成する頂点の座標が必要になります． ``ROWSx1x2`` というサイズの配列にこれらの頂点情報を格納します．ここで ROWS は頂点数を表す ``int32`` 型の変数です．ここでは4個の頂点を持つ多角形を黄色で描画します．
::

    pts = np.array([[10,5],[20,30],[70,20],[50,10]], np.int32)
    pts = pts.reshape((-1,1,2))
    img = cv2.polylines(img,[pts],True,(0,255,255))
    
.. Note:: 第3引数が ``False`` の時，閉包図形ではなく全ての点をつなぐ線を描画します．

.. Note:: ``cv2.polylines()`` 関数は複数の線を描くために使われます．描画したい全ての線のリストを関数に渡せtば，全ての線を独立して描画します．複数の線を描画する時に，線毎に ``cv2.line()`` 関数を実行するより高速に描画できます．

画像にテキストを追加:
------------------------
画像にテキストを描くには，以下の情報を指定する必要があります．
    * 書きたいテキストデータ
    * 書く場所の座標(テキストを書き始める位置の左下)
    * フォント ( OpenCVが提供するフォントの情報については **cv2.putText()** 関数のドキュメンテーションを参照してください)
    * フォントサイズ (文字のサイズ)
    * 一般的な情報(色，線の太さ，線の種類など ``lineType = cv2.LINE_AA`` が推奨されています
    
ここでは **OpenCV** という文字を白色で書き込みます．
::

    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(img,'OpenCV',(10,500), font, 4,(255,255,255),2,cv2.LINE_AA)

結果
----------
上記の全てのコードを実行すると，以下のような画像が生成されます．

         .. image:: images/drawing.jpg
              :alt: Drawing Functions in OpenCV
              :align: center 


補足資料
========================

1. 楕円関数内で使われる角度は，我々が使っている周傾斜角(circular angles)ではありません．詳細は `このディスカッション <http://answers.opencv.org/question/14541/angles-in-ellipse-function/>`_.  を参照してください．

課題
==============
#. OpenCVの描画機能を使って，OpenCVのロゴを作ってください．
