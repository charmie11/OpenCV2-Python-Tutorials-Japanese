﻿.. _Display_Image:

画像を扱う
*****************************

目的
======

.. container:: enumeratevisibleitemswithsquare

    * ここでは画像ファイルを読み込む方法，表示する方法，保存する方法を学びます
    * 以下の関数の使い方を学びます : **cv2.imread()**, **cv2.imshow()** , **cv2.imwrite()**
    * さらに，Matplotlibを使った画像の表示方法も学びます

Using OpenCV
====================

画像を読み込む
--------------

画像ファイルを読み込むには **cv2.imread()** という関数を使います．画像ファイルが作業ディレクトリ内に保存されている場合はファイル名のみを指定し，そうでない場合は絶対パスもしくは適切な相対パスで指定しなければいけません．

第2引数は画像の読み込み方法を指定するためのフラグです．

* cv2.IMREAD_COLOR : カラー画像として読み込む．画像の透明度は無視される．デフォルト値
* cv2.IMREAD_GRAYSCALE : グレースケール画像として読み込む
* cv2.IMREAD_UNCHANGED : アルファチャンネルも含めた画像として読み込む

.. note:: 上記のフラグを使う代わりに，単に1, 0, -1 の整数値を与えて指定することもできます．

以下のコードを見てください:
::
    
    import numpy as np
    import cv2
    
    # Load an color image in grayscale
    img = cv2.imread('messi5.jpg',0)
    
.. warning:: 画像ファイルのパスが間違っている場合，エラーは返しませんが ``print img`` とコマンドを実行すると ``None`` と表示されます

画像を表示する
-----------------

画像をウィンドウ上に表示するには **cv2.imshow()** という関数を使います．ウィンドウのサイズは自動で画像サイズに合わせられます．

第1引数は文字列型で指定するウィンドウ名です．第2引数は表示したい画像です．必要に応じて複数個のウィンドウを表示させることができますが，各ウィンドウには異なる名前をつけなければいけません．
::
    
    cv2.imshow('image',img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

上記のコードを(Fedora-Gnome machineで)実行した結果のスクリーンショットは以下のようになります．:

     .. image:: images/opencv_screenshot.jpg
              :alt: OpenCVのウィンドウのスクリーンショット
              :align: center 
   
**cv2.waitKey()** はキーボード入力を処理する関数です．引数は入力待ち時間でミリ秒単位で指定します．この関数は，指定された時間だけキーボード入力を受け付けます．入力待ちの間に何かのキーを打てば，プログラムはそれ以降の処理を実行します．引数に **0** を指定した時は，何かしらのキーを打つまでキー入力を無期限で待ち続けます．以下で説明しますが，特定のキー入力のみを待つ(例えば `a` のみを待つ)ことも可能です．

**cv2.destroyAllWindows()** は現在までに作られた全てのウィンドウを閉じる関数です．特定のウィンドウのみを閉じる場合は **cv2.destroyWindow()** 関数に閉じたいウィンドウ名を指定してください．.

.. note:: あらかじめ作成したウィンドウに後で読み込んだ画像を表示させたい時があるかもしれません．そのような時は，どのウィンドウがサイズの変更が可能か指定する事が出来ます．このウィンドウのサイズ変更機能は **cv2.namedWindow()** で指定できます．デフォルトでは ``cv2.WINDOW_AUTOSIZE`` というフラグが指定されています．フラグを ``cv2.WINDOW_NORMAL`` に指定すれば，サイズ変更が可能なウィンドウを作成できます．この機能は，画像のサイズが非常に大きい時やウィンドウにトラックバーを追加する時などに便利です．

以下のコードを見てください:
::
    
    cv2.namedWindow('image', cv2.WINDOW_NORMAL)
    cv2.imshow('image',img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
画像を保存する
---------------

画像を保存するには **cv2.imwrite()** 関数を使います．

第1引数は画像のファイル名，第2引数は保存したい画像です．
::
    
    cv2.imwrite('messigray.png',img)

この命令によって，この画像データがPNGファイル形式で作業ディレクトリに保存されます．

まとめると
---------------

以下が画像をグレースケール画像として読み込み，ウィンドウに表示し， 's' というキーを押すと画像を保存してから， `ESC` を押すと画像を保存せずに終了するプログラムです．
::
    
    import numpy as np
    import cv2
    
    img = cv2.imread('messi5.jpg',0)
    cv2.imshow('image',img)
    k = cv2.waitKey(0)
    if k == 27:         # wait for ESC key to exit
        cv2.destroyAllWindows()
    elif k == ord('s'): # wait for 's' key to save and exit
        cv2.imwrite('messigray.png',img)
        cv2.destroyAllWindows()
    
.. warning:: 64 bit マシンを使っている場合， ``k = cv2.waitKey(0)`` の部分を ``k = cv2.waitKey(0) & 0xFF`` と変更してください．

Matplotlibを使った画像の表示
==============================

MatplotlibはPythonのデータの可視化用ライブラリで，様々なデータの可視化方法を提供しています．ここでは，Matplotlibを使った画像の表示方法を学びます．Matplotlibを使えば，画像のズームや保存ができます．
::
    
    import numpy as np
    import cv2
    from matplotlib import pyplot as plt
    
    img = cv2.imread('messi5.jpg',0)
    plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
    plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
    plt.show()
    
ウィンドウのスクリーンショットは以下のようになります．:

     .. image:: images/matplotlib_screenshot.jpg
              :alt: Screenshot of Image Window in Matplotlib
              :align: center 
    
.. seealso:: Matplotlibを使えば，多くのオプションを指定できます．詳細については Matplotlib のドキュメントを参照してください．機能の内のいくつかはこのチュートリアルで扱います．

.. warning:: OpenCVで読み込んだカラー画像はBGRモードで読み込まれます．しかし， Matplotlib は画像をRGBモードで表示します．そのため，OpenCVを使って読み込んだ画像を表示しようとすると，色成分がおかしな表示になってしまいます．詳細については以下のexercisesを参照してください．

補足資料
======================

#. `Matplotlib プロットの種類と特徴(英語) <http://matplotlib.org/api/pyplot_api.html>`_

課題
==========

#. OpenCVで読み込んだカラー画像をMatplotLibを使って表示すると変な表示になってしまいます．`このディスカッション <http://stackoverflow.com/a/15074748/1134940>`_ を読み，その理由を理解してください．
