﻿.. _KNN:

k近傍法
**********************

*  :ref:`KNN_Understanding`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |KNN_1|      k近傍法の基本的な理解を得る
  =========== ===================================================================

  .. |KNN_1| image:: images/knn_icon1.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`KNN_OpenCV`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |KNN_2|      OpenCVのk近傍法を使った手書き文字認識
  =========== ===================================================================

  .. |KNN_2| image:: images/knn_icon2.jpg
                 :height: 90pt
                 :width:  90pt 
                 
.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   py_knn_understanding/py_knn_understanding
   py_knn_opencv/py_knn_opencv
