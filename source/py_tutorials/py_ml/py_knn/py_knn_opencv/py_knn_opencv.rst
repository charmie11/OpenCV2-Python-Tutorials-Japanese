﻿.. _KNN_OpenCV:

k近傍法を使った手書き文字認識
***********************************************

目的
=======

この章では
    * 前章で学んだk近傍法を使い，基本的なOCR(光学文字認識)を構築します．
    * OpenCVに含まれている数字とアルファベットのデータを使います．


手書き文字に対するOCR
============================

我々の目的は手書きの数字を読むためのアプリケーションの作成です．この目的を実現するためには，train_data(学習データ)と test_data(テストデータ)が必要です．OpenCVは5000個(0から9まで各数字が500個)の手書きの数字データを含む1枚の画像 `digits.png` ( ``opencv/samples/python2/data/`` フォルダ内に保存)を用意しています．各数字データは20x20のサイズの画像として連結されています．まず初めに，この連結された画像を5000個の画像に分割します．20x20のサイズの各数字データを，1行400画素のデータに変換します．この400画素のデータが全画素の画素値を特徴量として持つデータとなります．これは，我々が生成できる最も単純な特徴量です．各数字の最初の250個のデータをtrain_data(学習用のデータ)，残りの250個のデータをtest_data(テスト用のデータ)とします．
::

    import numpy as np
    import cv2
    from matplotlib import pyplot as plt

    img = cv2.imread('digits.png')
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    # Now we split the image to 5000 cells, each 20x20 size
    cells = [np.hsplit(row,100) for row in np.vsplit(gray,50)]

    # Make it into a Numpy array. It size will be (50,100,20,20)
    x = np.array(cells)

    # Now we prepare train_data and test_data.
    train = x[:,:50].reshape(-1,400).astype(np.float32) # Size = (2500,400)
    test = x[:,50:100].reshape(-1,400).astype(np.float32) # Size = (2500,400) 

    # Create labels for train and test data
    k = np.arange(10)
    train_labels = np.repeat(k,250)[:,np.newaxis]
    test_labels = train_labels.copy()

    # Initiate kNN, train the data, then test it with test data for k=1
    knn = cv2.KNearest()
    knn.train(train,train_labels)
    ret,result,neighbours,dist = knn.find_nearest(test,k=5)

    # Now we check the accuracy of classification
    # For that, compare the result with test_labels and check which are wrong
    matches = result==test_labels
    correct = np.count_nonzero(matches)
    accuracy = correct*100.0/result.size
    print accuracy


これで基本的な文字認識アプリケーションの準備が出来ました．この例では91%の精度を達成しました．精度向上の方法として，学習データ，特に誤ったデータ，を増やすことが挙げられます．アプリケーションを起動するたびにデータの学習を行う代わりに学習結果を保存するようにします．そうすると，以降は保存した学習データを読み込めば文字認識を実行できるからです．データの保存はNumpyの関数を幾つか，例えばnp.savetxt, np.savez, np.load等を使えば可能です．詳細については各関数のドキュメントを参照してください．
::

    # save the data
    np.savez('knn_data.npz',train=train, train_labels=train_labels)

    # Now load the data
    with np.load('knn_data.npz') as data:
        print data.files
        train = data['train']
        train_labels = data['train_labels']

私のコンピュータでは保存データは4.4MBの容量になりました．特徴量として画像の画素値(uint8型のデータ)を使っているので，データを np.uint8型に変換してから保存すると良いでしょう．そうすると，データ容量はたったの1.1 MBになりました．データを読み込むときに，忘れずにfloat32型のデータに変換してください．

英語のアルファベットの文字認識
========================================

次に，同様の文字認識を英語のアルファベットに対して適用しますが，データと特徴量に多少違いがあります．文字データは画像ではなく ``opencv/samples/cpp/``  フォルダ内の ``letter-recognition.data`` というデータとして提供されています．

上記のデータファイルを開くと，一見意味不明な20000行のデータの羅列に見えるかと思います．各行のデータは，一列目に各アルファベットに割り当てられるラベルの値，続く16個の数値は異なる特徴量を表しています．これらの特徴量は `UCI Machine Learning Repository <http://archive.ics.uci.edu/ml/>`_. You can find the details of these features in `this page <http://archive.ics.uci.edu/ml/datasets/Letter+Recognition>`_ から得たものです．

20000個のサンプルが使用可能なので，最初の10000個のデータを学習用，残りの10000個のデータをテスト用のデータとしましょう．アルファベットを直接扱う事が出来ないため，アルファベットをASCII文字に変換する必要があります．
::

    import cv2
    import numpy as np
    import matplotlib.pyplot as plt

    # Load the data, converters convert the letter to a number
    data= np.loadtxt('letter-recognition.data', dtype= 'float32', delimiter = ',', 
                        converters= {0: lambda ch: ord(ch)-ord('A')})

    # split the data to two, 10000 each for train and test
    train, test = np.vsplit(data,2)

    # split trainData and testData to features and responses
    responses, trainData = np.hsplit(train,[1])
    labels, testData = np.hsplit(test,[1])

    # Initiate the kNN, classify, measure accuracy.
    knn = cv2.KNearest()
    knn.train(trainData, responses)
    ret, result, neighbours, dist = knn.find_nearest(testData, k=5)

    correct = np.count_nonzero(result == labels)
    accuracy = correct*100.0/10000
    print accuracy

私のマシンで実行した結果，93.22%の精度となりました．前述しましたが，精度の向上を図るのであれば各レベルごとにエラーデータを繰り返し追加してください．

補足資料
=======================

課題
=============

