﻿.. _PY_Table-Of-Content-ML:

機械学習
********************

*  :ref:`KNN`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |ML_KNN|    k近傍法を使った分類とk近傍法を使った手書き文字認識について学びます．
  =========== ===================================================================

  .. |ML_KNN| image:: images/knnicon.png
                 :height: 90pt
                 :width:  90pt


*  :ref:`SVM`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |ML_SVM|    Support Vector Machine (SVM)の概念を理解します．
  =========== ===================================================================

  .. |ML_SVM| image:: images/svmicon.png
                 :height: 90pt
                 :width:  90pt

*  :ref:`KMeans_Clustering`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |ML_KM|     データを指定したクラスタ数にグループ分けするためにK-Meansクラスタリングを使います．また，K-Meansクラスタリングを使った色の量子化の方法を学びます．
  =========== ===================================================================

  .. |ML_KM| image:: images/kmeansicon.jpg
                 :height: 90pt
                 :width:  90pt


.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   ../py_knn/py_knn_index
   ../py_svm/py_svm_index
   ../py_kmeans/py_kmeans_index
