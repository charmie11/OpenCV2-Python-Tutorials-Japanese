﻿.. _KMeans_Clustering:

K-Meansクラスタリング
*********************

*  :ref:`KMeans_Clustering_Understanding`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |KM_1|      K-Meansクラスタリングの直観的理解を得るために読む
  =========== ===================================================================

  .. |KM_1| image:: images/kmeans_begin.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`KMeans_OpenCV`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |KM_2|      OpenCVのK-Means関数を試してみよう
  =========== ===================================================================

  .. |KM_2| image:: images/kmeans_demo.jpg
                 :height: 90pt
                 :width:  90pt 
                 
.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   py_kmeans_understanding/py_kmeans_understanding
   py_kmeans_opencv/py_kmeans_opencv
