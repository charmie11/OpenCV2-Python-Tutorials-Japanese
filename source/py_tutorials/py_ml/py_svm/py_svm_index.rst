﻿.. _SVM:

サポートベクターマシン(SVM: Support Vector Machines)
****************************************************

*  :ref:`SVM_Understanding`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |SVM_1|      SVMの基本理解を得る
  =========== ===================================================================

  .. |SVM_1| image:: images/svm_icon1.jpg
                 :height: 90pt
                 :width:  90pt


*  :ref:`svm_opencv`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |SVM_2|      OpenCVが提供するSVMの機能を使ってみよう
  =========== ===================================================================

  .. |SVM_2| image:: images/svm_icon2.jpg
                 :height: 90pt
                 :width:  90pt
                 
.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   py_svm_basics/py_svm_basics
   py_svm_opencv/py_svm_opencv
   

