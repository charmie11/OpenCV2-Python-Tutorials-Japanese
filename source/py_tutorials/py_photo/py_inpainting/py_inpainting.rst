﻿.. _inpainting:


画像のInpainting
**********************

目的
======

このチュートリアルでは
    * 古い写真上の小さなノイズや引っかき傷などをinpaintingと呼ばれる技術によって消す方法を学びます．
    * OpenCVのinpaintingの関数の使い方を学びます．

基礎
===========

折れ目や傷がついてしまった古い写真を持っている人は多いでしょう．そのような写真を修復しようと考えたことはあるでしょうか?そのような傷をペイントツールを使って簡単に消すことは不可能です．このような画像の修復には画像のinpaintingと呼ばれる技術が使われます．基本的なアイディアはシンプルです: このような傷を周囲の画素情報を使って置換することで周囲の画素のように見えるわけです．以下に示す画像(`Wikipedia <http://en.wikipedia.org/wiki/Inpainting>`_ から引用)を考えてみてください:

    .. image:: images/inpaint_basics.jpg
        :alt: Inpainting example
        :align: center
        
この目的のために数多くのアルゴリズムが設計されており，OpenCVは二つのアルゴリズムを実装しています．どちらのアルゴリズムも **cv2.inpaint()** という関数から使う事が出来ます．

第一のアルゴリズムはAlexandru Teleaが2004年に発表した **"An Image Inpainting Technique Based on the Fast Marching Method"** という論文で提案されたアルゴリズムです．このアルゴリズムはFast Marching Methodを基にしたアルゴリズムです．上に示した画像内のある領域をinpaintingすることを考えます．このアルゴリズムはこの領域の境界から内側に向かって徐々に傷を修復していきます．修復する近傍領域上のある一画素の値を，その周囲の画素の中で画素値が既に分かっている画素の画素値の重み付き和で置換します．この時，重みの値をどのように決めるかが重要となります．注目画素に近い画素，境界線の法線に近い画素，境界線上の画素に対してより大きい重みを与えるようにします．ある画素を修復したら，Fast Marching Method(FMM)を使って次の最近傍点に移動します．FMMを使う事で画素値が既知の画素(傷がついていない，もしくは修復済みの画素)に近い画素から順番に修復していくことを意味し，手作業で行うヒューリスティックな手法のように作用します．このアルゴリズムは ``cv2.INPAINT_TELEA`` フラグをONにすると使えます．

第二のアルゴリズムはBertalmio, Marcelo, Andrea L. Bertozzi, Guillermo Sapiroが2001年に発表した **"Navier-Stokes, Fluid Dynamics, and Image and Video Inpainting"** という論文で提案された手法です．このアルゴリズムは流体力学に基づくアルゴリズムで辺微分方程式を利用したアルゴリズムです．基本的な原理はヒューリスティックな方法です．まず初めに画素値が既知の領域から未知の領域へエッジに沿って探索します(エッジは連続的なものであるから)．このエッジは修復領域の境界線の法線ベクトルに相当し，等輝度線上を移動します．これを実現するために流体力学の手法を使います．次に，その領域(等輝度線に囲まれる領域?)内の輝度の分散が最小となるように欠損した画素の色を修復します．このアルゴリズムは ``cv2.INPAINT_NS`` フラグをONにすると使えます．

コード(実装)
===============

inpaintingを行うには，入力画像と同じサイズのマスク画像を作成する必要があります．このマスク画像中で非ゼロの値を持つ画素が修復するべき画素を表します．それ以外は単純です．私が故意にペイントツールで追加した黒い線の除去を行いましょう．
::

    import numpy as np
    import cv2

    img = cv2.imread('messi_2.jpg')
    mask = cv2.imread('mask2.png',0)

    dst = cv2.inpaint(img,mask,3,cv2.INPAINT_TELEA)

    cv2.imshow('dst',dst)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    

以下に示す結果を見てください．左上の画像が入力劣化画像，右上の画像がマスク画像，左下の画像が第一のアルゴリズムによる復元画像，右下の画像が第二のアルゴリズムによる復元画像です

    .. image:: images/inpaint_result.jpg
        :alt: Inpainting result
        :align: center
        
        
補足資料
=========================

#. Bertalmio, Marcelo, Andrea L. Bertozzi, and Guillermo Sapiro. "Navier-stokes, fluid dynamics, and image and video inpainting." In Computer Vision and Pattern Recognition, 2001. CVPR 2001. Proceedings of the 2001 IEEE Computer Society Conference on, vol. 1, pp. I-355. IEEE, 2001.

#. Telea, Alexandru. "An image inpainting technique based on the fast marching method." Journal of graphics tools 9.1 (2004): 23-34.


課題
================

#. OpenCVのサンプルに対話的なinpaintingのサンプル ``samples/python2/inpaint.py`` が含まれています．試しに使ってみてください．

#. `Content-Aware Fill <http://www.youtube.com/watch?v=ZtoUiplKa2A>`_ というAdobe Photoshopのinpaintingの機能の動画があります．GIMPにも同様の機能である "Resynthesizer" があります．試しに使ってみると楽しめるでしょう．
