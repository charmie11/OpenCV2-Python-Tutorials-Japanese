﻿.. _PY_Table-Of-Content-Photo:


Computational Photography
--------------------------------

ここではノイズ除去のようなComputational Photographyに関係するOpenCVの関数を学びます．


*  :ref:`non_local_means`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |photo_1|   画像中のノイズを除去するNon-Local Means Denoisingという技術を学びます．

  =========== ======================================================

  .. |photo_1|  image:: images/nlm_icon.jpg
                 :height: 90pt
                 :width:  90pt
                 

*  :ref:`inpainting`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |photo_2|   黒い穴や線ができてしまった古い劣化してしまった写真を持っていますか?その写真を使いましょう．画像のinpaintingと呼ばれる技術を使いそのような写真を復元しましょう．

  =========== ======================================================

  .. |photo_2|  image:: images/inpainticon.jpg
                 :height: 90pt
                 :width:  90pt                 
                 
               
.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   ../py_non_local_means/py_non_local_means
   ../py_inpainting/py_inpainting
