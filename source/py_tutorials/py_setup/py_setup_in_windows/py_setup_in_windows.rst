﻿.. _Install-OpenCV-Python-in-Windows:

OpenCV-PythonをWindowsにインストール
*******************************************

目的
======

このチュートリアルでは
    * Windows上にOpenCV-Pythonをインストールする方法を学びます．
    
*以下のインストール方法はWindows 7(64ビット)とVisual Studio 2010とVisual Studio 2012で動作確認をしました．スクリーンショット画面はVisual Studio 2012のものです．*

プレビルトバイナリファイルからOpenCVをインストール
=====================================================

1. 以下のPythonのパッケージをダウンロードし，デフォルトのフォルダにインストールします．

    1.1. `Python-2.7.x <http://python.org/ftp/python/2.7.5/python-2.7.5.msi>`_.

    1.2. `Numpy <http://sourceforge.net/projects/numpy/files/NumPy/1.7.1/numpy-1.7.1-win32-superpack-python2.7.exe/download>`_.

    1.3. `Matplotlib <https://downloads.sourceforge.net/project/matplotlib/matplotlib/matplotlib-1.3.0/matplotlib-1.3.0.win32-py2.7.exe>`_ (*Matplotlib is optional, but recommended since we use it a lot in our tutorials*).

2. 全てのパッケージをデフォルトのフォルダにインストールしてください．Pythonは **C:/Python27/** にインストールされます．

3. インストールが終了したらPythonのターミナルを開きます．ターミナル上で ``import numpy`` と実行してNumpyが正しく動作することを確認してください．

4. 最新版のOpenCVを `sourceforge site <http://sourceforge.net/projects/opencvlibrary/files/opencv-win/2.4.6/OpenCV-2.4.6.0.exe/download>`_ からダウンロードして解凍してください．

7.  **opencv/build/python/2.7** フォルダに移動してください．

8. フォルダ内の **cv2.pyd** を **C:/Python27/lib/site-packeges** フォルダ内にコピーしてください．

9. 改めてPythonターミナルを開き，以下のコードを実行してください．

    >>> import cv2
    >>> print cv2.__version__

エラーメッセージが無く結果が表示されればOpenCV-Pythonのインストールに成功したことになります!!!


OpenCVをソースからビルド
===============================
1. Visual StudioとCMあけをインストールしてください．

    1.1. `Visual Studio 2012 <http://go.microsoft.com/?linkid=9816768>`_

    1.2. `CMake <http://www.cmake.org/files/v2.8/cmake-2.8.11.2-win32-x86.exe>`_

2. 以下の必要なPythonパッケージをダウンロードし，デフォルトフォルダにインストールしてください．

    2.1. `Python 2.7.x <http://python.org/ftp/python/2.7.5/python-2.7.5.msi>`_

    2.2. `Numpy <http://sourceforge.net/projects/numpy/files/NumPy/1.7.1/numpy-1.7.1-win32-superpack-python2.7.exe/download>`_

    2.3. `Matplotlib <https://downloads.sourceforge.net/project/matplotlib/matplotlib/matplotlib-1.3.0/matplotlib-1.3.0.win32-py2.7.exe>`_ (*Matplotlib is optional, but recommended since we use it a lot in our tutorials.*)

.. note:: この説明では32bit版のPythonパッケージを使っています．64bit版のOpenCVを使いたいのであればPythonパッケージも64bit版のものをインストールしてください．問題はNumpyが公式に64bitマシンをサポートしていないため，Numpyを自分でビルドしなければいけない点です．そのためにはPythonのビルドに使うコンパイラと同じコンパイラを使わなければいけません．Pythonターミナルを開くとコンパイラの詳細が表示されます． `ここ <http://stackoverflow.com/q/2676763/1134940>`_ に詳しい説明が書いてあります．Numpyをビルドする時に使うものと同じVisual Studioをインストールしてある必要があります．

.. note:: 64bit版のPythonパッケージをインストールするもう一つの方法は `Anaconda <http://www.continuum.io/downloads>`_, や `Enthought <https://www.enthought.com/downloads/>`_ のような第三者によって提供されているパッケージを使う方法です．インストールされるファイルのサイズは大きくなりますが，必要とされるものを全てインストールできます．このようなサードパーティのパッケージ群は32bit版も同様にインストール可能です．

3. PythonとNumpyが正しく動作する事を確認してください．．

4. OpenCVのソースコードをダウンロードしてください． `Sourceforge <http://sourceforge.net/projects/opencvlibrary/>`_ から公式リリース版のソースコードをダウンロードできます．もしくは`Github <https://github.com/Itseez/opencv>`_ からソースコードをダウンロードできます．

5. ``opencv`` というフォルダ内に解凍し， ``build`` という名前のフォルダを作成してください．

6. CMake-GUIを開いてください (*スタート > 全てのプログラム > CMake-gui*)

7. 以下の画像に示しているようにフィールドに値を入れてください:
    
    7.1. **Browse Source...** ボタンをクリックし，先ほどの ``opencv`` フォルダを指定してください．
    
    7.2. **Browse Build...** ボタンをクリックし，先ほど作成した ``build`` フォルダを指定してください．
    
    7.3. **Configure** ボタンをクリックしてください．

        .. image:: images/Capture1.jpg
            :alt: capture1
            :align: center

    
    7.4. コンパイラ選択するためのウィンドウが表示されるので，正しいコンパイラを選択して(ここではVisual Studio 11を選択します) **Finish** ボタンをクリックしてください．
    
        .. image:: images/Capture2.png
            :alt: capture2
            :align: center        

    
    7.5. 解析が終わるまで待ってください．
    
8. 解析が終了すると全フィールドが赤くマークされます． **WITH** フィールドをクリックし，サブフィールドを展開させ，以下の図を参考に必要なフィールドをマークしてください:

    .. image:: images/Capture3.png
        :alt: capture3
        :align: center

        
9. 必要なフィールドのマークが終わったら **BUILD** フィールドをクリックして展開してください．最初の幾つかのフィールドがビルド方法の構成です．以下の画像を参考にしてください:

    .. image:: images/Capture5.png
        :alt: capture5
        :align: center

        
10. 以降のフィールドはビルドするモジュールを指定するためのものです．現時点(OpenCV3.0.0の時点?)でOpenCV-PythonはGPU関連のモジュールはサポートしていないので，時間節約のためこれらのモジュールのビルドをする必要はありません(必要であればビルドしてください)．以下の画像を参考にしてください:

    .. image:: images/Capture6.png
        :alt: capture6
        :align: center

        
11. 次に， **ENABLE** フィールドをクリックして展開し， **ENABLE_SOLUTION_FOLDERS** のチェックが外れていることを確認してください(ソリューションフォルダはVisual Studio Express版ではサポートされていません)．以下の画像を参考にしてください:

    .. image:: images/Capture7.png
        :alt: capture7
        :align: center    

        
12. また， **PYTHON** フィールドの全てがチェックされているか確認してください(ただし， PYTHON_DEBUG_LIBRARY は無視してください)．以下の画像を参考にしてください:

    .. image:: images/Capture80.png
        :alt: capture80
        :align: center  

        
13. 最後に **Generate** ボタンを押してください．

14. それでは **opencv/build** フォルダを見てください． **OpenCV.sln** ファイルができているはずです．早速Visual Studioで開いてください．

15. ビルドモードを **Debug** ではなく **Release** に変更してください．

16. ソリューションエクスプローラー内の **Solution** (もしくは **ALL_BUILD**) を右クリックし，ビルドしてください．終了までにある程度時間がかかります．

17. 次に **INSTALL** を右クリックしビルドするとOpenCV-Pythonがインストールされます．

    .. image:: images/Capture8.png
        :alt: capture8
        :align: center
        
        
18. Pythonターミナルを開き， ``import cv2`` と実行してください．何もエラーが表示されなければインストールが正しくできたことを意味します．

.. note:: この例ではTBB, Eigen, Qtなどのサポート無しでインストールしました．ここで説明をするのは大変なので，さらなる詳細を説明するビデオが追加されるのを待つか，自分の手で色々いじってみてください．


補足資料
========================== 


課題
============

1. Windowsマシンを持っているのであれば，OpenCVをソースからコンパイルしてみてください．その際に，あらゆるカスタマイズを試してみてください．何かエラーなどが起きればOpenCVのフォーラムを訪問し自分の問題を説明してください．
