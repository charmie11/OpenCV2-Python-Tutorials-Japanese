﻿.. _Intro:


OpenCV-Pythonチュートリアルとは
*******************************

OpenCV
===============

OpenCVはIntel社で1999年に **Gary Bradsky** によって開発が開始され，2000年に最初にリリースされました． **Vadim Pisarevsky** がIntel社のロシアソフトウェアOpenCVチームの管理をするためにGary Bradskyの仲間になりました．2005年にOpenCVは2005年DARPA Grand Challengeにて勝利したStanleyという車で使用されました．その後，ｆWillow Garageのサポートの下Gary BradskyとVadimPisarevskyに率いられ開発が続けられました．現在ではOpenCVはComputer Visionと機械学習に関する大量のアルゴリズムをサポートしており，日に日に進歩しています．

現在OpenCVはC++, Python, Javaといった様々なプログラミング言語をサポートし，Windows, Linux, OS X, Android, iOSといった異なるプラットフォーム上で使用可能です．また，高速なGPU計算を可能とするためにCUDAとOpenCVを基にしたインタフェースも開発中です．

OpenCV-PythonはOpenCVのPythonのAPIです．OpenCVのC++ APIの最高品質とPython言語を組み合わせたものです．


OpenCV-Python
===============

Pythonは **Guido van Rossum** によって開発が始められた汎用プログラミング言語であり，その簡易性とコードの可読性によって短時間の間にとても人気がある言語となりました．可読性を失うことなく少ない行数のコードでアイディアを実現することが可能です．

C/C++といった他の言語とひかくすると，Pythonは低速です．しかしPythonのもう一つの重要な特徴はC/C++と共に簡単に拡張できる点が挙げられます．この特徴はC/C++で計算量の多いコードを書きPythonのラッパーを作成する手助けになり，これによってラッパーをPythonのモジュールとして使うことができます．この特徴は二つの利点を与えてくれます: まず初めに元のC/C++のコードと同程度の速度で実行でき(バックグラウンドでは本当のC++のコードが走るからです)，第二にPythonで実装するのがとても簡単だからです．これがOpenCV-Pythonが動作する理由であり，元のC++の実装についてのPythonラッパーだということです．

さらに，Numpyのサポートによってタスクがさらに簡単になります． **Numpy** は数値計算のために高度に最適化されたライブラリで，MatLab形式のシンタックスを提供します．OpenCVの全array構造とNumpyのarrayは互いに変換しあえます．つまり，どのような処理であれNumpyを使って計算できますし，OpenCVと組み合わせられます．これに加えて，SciPy, Matplotlibといったその他のライブラリがNumpyの使用のサポートをします．

よって，OpenCV-PythonはComputer Visionの問題のプロトタイプを早く実装するための適切なツールと言えます．


OpenCV-Pythonチュートリアル
=============================

OpenCVはOpenCV-Pythonで使用可能な様々な関数の説明をする新しいチュートリアルのセットを紹介します．
 **このガイドはOpenCVのバージョン3.xを主に対象としています** (大半のチュートリアルはOpenCV 2.xでも同様に動作します)．

本チュートリアルはPythonとNumｐｙに関する知識を持っていることを前提としているため，それらについては扱いません． **特に，OpenCV-Pythonで最適化されたコードを書くために，Numpyに関する知識は必須です．**

このチュートリアルは *Abid Rahman K.* が2013年のGoogle Summer of Codeの一部として *Alexander Mordvintsev* の下スタートしました．


OpenCVはあなたを必要としている !!!
=======================================

OpenCVはオープンソースであるため，OpenCVに対する貢献をしてくれる方を歓迎します．それは，このチュートリアルに関しても同様です．

もしこのチュートリアル内に間違いがあれば，例えどんな些細なスペルミスであっても，コードや概念などの大きなミスであっても気兼ねなく修正してください．

これはオープンソースプロジェクトに対する貢献を始めたばかりの人にとって良いタスクになるでしょう．githubからOpenCVをforkし，必要な修正を行い，OpenCVに対してプルリクエストを送るだけでいいのです．OpenCVの開発者はあなたのプルリクエストを確認しあなたに対して重要なフィードバックを送ります．プルリクエストの確認者から許可が得られれば，あなたのプルリクエストがOpenCVに統合されます．このようにしてあなたはオープンソースプロジェクトの貢献者になるのです．その他のチュートリアルやドキュメント等についても同様です．

OpenCV-Pythonに新しいモジュールが追加される度にこのチュートリアルは拡張されるべきです．特定のアルゴリズムに関して詳しい人がそのアルゴリズムの基礎理論やそのアルゴリズムの基本的な使い方を教えてくれるコードを含むチュートリアルをOpenCVに投稿できるのです．

我々が **一緒になって** このプロジェクトを成功に導けることを覚えておいてください．


貢献者
=================

OpenCV-Pythonに対してチュートリアルを投稿してくれた貢献者のリストを以下に示します．

1. Alexander Mordvintsev (GSoC-2013 mentor)
2. Abid Rahman K. (GSoC-2013 intern)


補足資料
=======================

1. A Quick guide to Python - `A Byte of Python <http://swaroopch.com/notes/python/>`_
2. `Basic Numpy Tutorials <http://wiki.scipy.org/Tentative_NumPy_Tutorial>`_
3. `Numpy Examples List <http://wiki.scipy.org/Numpy_Example_List>`_
4. `OpenCV Documentation <http://docs.opencv.org/>`_
5. `OpenCV Forum <http://answers.opencv.org/questions/>`_
