﻿.. _Install-OpenCV-Python-in-Fedora:

OpenCV-PythonのFedoraへのインストール
*************************************

目的
======

このチュートリアルでは
    * Fedora上でのOpenCV-Pythonのインストール方法を学びます．以下の手順はFedora 18 (64-bit)とFedora 19 (32-bit)で動作確認をしました．

はじめに
==================

OpenCV-PythonをFedoraにインストールする方法はFedoraレポジトリのプレビルトバイナリを使う方法とソースからコンパイルする方法の二つがあります．本チュートリアルでは両方を取り上げます．

もう一つの重要な要素として必要なライブラリが挙げられます．OpenCV-Python自体は **Numpy** のみ(これ以外の依存関係については後で話します)を必要としますが，本チュートリアルでは図のプロット のために **Matplotlib** を使います．Matplotlibのインストールは必須ではありませんが強くお勧めします．同様に，対話的なPythonターミナルとして **IPython** の使用もお勧めします．

プレビルトバイナリを使ったインストール
===================================================

root権限がある状態でターミナルから以下のコマンドを使って全てのパッケージをインストールしてください．

    .. code-block:: bash
    
        $ yum install numpy opencv*
    
Pythonターミナル(もしくはIPython)を起動し，以下のコマンドを実行してください．

    .. code-block:: python
    
        >>> import cv2
        >>> print cv2.__version__
    
もし何のエラーもなく結果が表示されればOpenCV-Pythonのインストールの成功です．

非常に簡単ですが，問題もあります．Yumレポジトリで常に最新版のOpenCVが利用可能とは限らない事です．例えば，このチュートリアルが書かれたタイミングではOpenCVの最新版が2.4.6であるのに対し，Yumレポジトリで利用可能なOpenCVのバージョンは2.4.5です．PythonのAPIを見ると最新版は常により良い機能を提供しています．また，ドライバやffmpeg, gstreamerパッケージに依存したカメラや動画に関する問題が起きるかもしれません．

そのため，私は次に紹介するソースからコンパイルする方法が個人的に好きです．また，あなたがOpenCVに対して何かしらの貢献をする時にはソースからコンパイルする必要があります．


ソースからインストール
===============================

ソースからコンパイルする方法は初めは複雑に感じるかもしれませんが，一度成功すると何も複雑に感じなくなるでしょう．

まず初めにOpenCVのコンパイルに必要なライブラリ・パッケージのインストールを行いましょう．必須ではないものはインストールをしなくても問題ありません．


必須インストール
---------------------------

インストールの構成を決定するためには **CMake** ，コンパイルには **GCC**，Pythonへの拡張のために **Python-devel** と **Numpy** が必要です．

    .. code-block:: bash
    
        yum install cmake
        yum install python-devel numpy
        yum install gcc gcc-c++


次に，GUI, カメラ(libdc1394, libv4l)，動画(ffmpeg, gstreamer)等のサポートをするためにGTKをインストールします．

    .. code-block:: bash

        yum install gtk2-devel
        yum install libdc1394-devel
        yum install libv4l-devel
        yum install ffmpeg-devel
        yum install gstreamer-plugins-base-devel


任意インストール
--------------------------

上記の依存ライブラリ・パッケージをインストールすればOpenCVのコンパイルには十分ですが，OpenCVによって実現したいことに応じて追加で依存ライブラリ・パッケージが必要になります．そのような任意の依存ライブラリ・パッケージのリストを示します．インストールの有無はあなた次第です :)
        
OpenCVはPNG, JPEG, JPEG2000, TIFF, WebP といった様々な画像フォーマットを扱えますが，これらのフォーマットを最新のライブラリで扱いたければ以下のパッケージをインストールしてください．

    .. code-block:: bash
        
        yum install libpng-devel
        yum install libjpeg-turbo-devel
        yum install jasper-devel
        yum install openexr-devel
        yum install libtiff-devel
        yum install libwebp-devel
        
OpenCVの様々な関数が **Intel's Threading Building Blocks** (TBB)を使って並列化できます．この機能を使いたければ，まず初めにTBBをインストールする必要があります(CMakeでビルド構成を決める時に ``-D WITH_TBB=ON`` とすることを忘れないでください．詳細については後で述べます．)．

    .. code-block:: bash
        
        yum install tbb-devel
        
OpenCVは最適化された数学的処理を行うために **Eigen** というライブラリを使っています．Eigenをインストールしているのであれば使いましょう(CMakeでビルド環境の構成をする時に ``-D WITH_EIGEN=ON`` とすることを忘れないでください．詳細については後で述べます．)．

    .. code-block:: bash
        
        yum install eigen3-devel

OpenCVの **ドキュメント** をオフライン時にも利用したければ， **Sphinx** (ドキュメント作成ツール)と **pdflatex** (ドキュメントのPDF版が必要なら)のインストールが必要です．作成されるドキュメントは検索機能が搭載されているHTMLファイルになるため，インターネット環境が無くても高速に閲覧可能です(CMakeでビルド環境の構成をする時に ``-D BUILD_DOCS=ON`` とすることを忘れないでください．詳細については後で述べます．)．

    .. code-block:: bash
        
        yum install python-sphinx
        yum install texlive
        

OpenCVのダウンロード
-----------------------

次にOpenCVのソースコードをダウンロードします． `sourceforgeのサイト<http://sourceforge.net/projects/opencvlibrary/>`_ から最新版のソースコードをダウンロードし解凍します．

もしくはgithubのOpenCVのレポジトリから最新版のソースコードをダウンロードしても良いでしょう(もしOpenCVに対して貢献をしたいのであれば，この方法を選択してください．OpenCVを常に最新版に保てます．)．githubのレポジトリからダウンロードするには，まず初めに **Git** をインストールする必要があります．以下のコードでGitのインストールのOpenCVのソースコードのダウンロードができます．

    .. code-block:: bash
    
        yum install git
        git clone https://github.com/Itseez/opencv.git
        
ホームディレクトリもしくは指定したディレクトリの直下に ``OpenCV`` というディレクトリが生成されます．レポジトリのクローン作成にはインターネットの接続状態に応じて時間がかかるかもしれません．

次にOpenCVディレクトリの直下に ``build`` という名前のディレクトリを作成してください．

    .. code-block:: bash
    
        mkdir build
        cd build        
        
        
configureとインストール
----------------------------

既に必要な依存パッケージをインストールしているので，OpenCVのインストールを行いましょう．CMakeを使ってビルド環境の構成を決定します．CMakeによる構成によってインストールするモジュール，インストール先，追加ライブラリ，ドキュメントやサンプルプログラムをインストールするか等の設定を指定します．以下のコードは一般的な構成のためのコードです(``build`` ディレクトリで実行します)．

    .. code-block:: bash
    
        cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local ..
        
このコマンドはビルドタイプを "Release Mode" とし，インストール先を ``/usr/local`` と設定します．各オプションの前に ``-D`` と書き，最後に ``..`` と書いている点に注目してください．コマンドのフォーマットは以下のようになっています:

    .. code-block:: bash
        
        cmake [-D <flag>] [-D <flag>] ..
        
指定したいフラグの数だけ指定ができますが，必ず ``-D`` をフラグの前につけてください．

このチュートリアルではTBBとEigenのサポートをつけてOpenCVをインストールします．ドキュメントも作成しますが，テストとサンプルプログラムのビルドは行いません．OpenCV-Pythonがサポートしていないため，GPU関連のモジュールもOFFにします．

*(以下のコマンドは1行のコマンドで実行できますが理解のしやすさのため複数に分割して示しています．)*

* TBBとEigenのサポート:

    .. code-block:: bash
        
        cmake -D WITH_TBB=ON -D WITH_EIGEN=ON ..
        
* ドキュメントを作成するがテストとサンプルは作成しない:

    .. code-block:: bash
    
        cmake -D BUILD_DOCS=ON -D BUILD_TESTS=OFF -D BUILD_PERF_TESTS=OFF -D BUILD_EXAMPLES=OFF ..
        
* GPUに関する全モジュールをビルドしない:

    .. code-block:: bash
        
        cmake -D WITH_OPENCL=OFF -D WITH_CUDA=OFF -D BUILD_opencv_gpu=OFF -D BUILD_opencv_gpuarithm=OFF -D BUILD_opencv_gpubgsegm=OFF -D BUILD_opencv_gpucodec=OFF -D BUILD_opencv_gpufeatures2d=OFF -D BUILD_opencv_gpufilters=OFF -D BUILD_opencv_gpuimgproc=OFF -D BUILD_opencv_gpulegacy=OFF -D BUILD_opencv_gpuoptflow=OFF -D BUILD_opencv_gpustereo=OFF -D BUILD_opencv_gpuwarping=OFF ..
        
* インストール先とビルドタイプの設定:

    .. code-block:: bash
    
        cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local ..
        
cmakeの設定をする度に構成結果がターミナルに表示されます．最終的な設定が終わった後に，以下のフィールドがチェックされていることを確認してください(以下に幾つか重要な部分を表示します)．これらのフィールドがあなたのシステム上で正しく設定されていなければいけません．さもなければ何かしらの問題が発生する可能性があります．そのような時は上記のステップを正しく行ったか確認してください．

    .. code-block:: bash

        --   GUI:     
        --     GTK+ 2.x:                    YES (ver 2.24.19)
        --     GThread :                    YES (ver 2.36.3)

        --   Video I/O:
        --     DC1394 2.x:                  YES (ver 2.2.0)
        --     FFMPEG:                      YES
        --       codec:                     YES (ver 54.92.100)
        --       format:                    YES (ver 54.63.104)
        --       util:                      YES (ver 52.18.100)
        --       swscale:                   YES (ver 2.2.100)
        --       gentoo-style:              YES
        --     GStreamer:                  
        --       base:                      YES (ver 0.10.36)
        --       video:                     YES (ver 0.10.36)
        --       app:                       YES (ver 0.10.36)
        --       riff:                      YES (ver 0.10.36)
        --       pbutils:                   YES (ver 0.10.36)

        --     V4L/V4L2:                    Using libv4l (ver 1.0.0)

        --   Other third-party libraries:
        --     Use Eigen:                   YES (ver 3.1.4)
        --     Use TBB:                     YES (ver 4.0 interface 6004)

        --   Python:
        --     Interpreter:                 /usr/bin/python2 (ver 2.7.5)
        --     Libraries:                   /lib/libpython2.7.so (ver 2.7.5)
        --     numpy:                       /usr/lib/python2.7/site-packages/numpy/core/include (ver 1.7.1)
        --     packages path:               lib/python2.7/site-packages

        --   Documentation:
        --     Build Documentation:         YES
        --     Sphinx:                      /usr/bin/sphinx-build (ver 1.1.3)
        --     PdfLaTeX compiler:           /usr/bin/pdflatex
        -- 
        --   Tests and samples:
        --     Tests:                       NO
        --     Performance tests:           NO
        --     C/C++ Examples:              NO  
        
ここで表示していない設定はたくさんありますが，詳細な説明はここでは行いません．

CMakeでビルド環境を作った後は ``make`` コマンドでビルドをし， root権限で ``make install`` コマンドを実行する事でインストールを行います．

    .. code-block:: bash
        
        make
        su
        make install
        
これでインストールは終了です．全ファイルが ``/usr/local/`` ディレクトリにいストールされています．Pythonから利用するためにはOpenCVのモジュールをPythonが発見できなければいけません．このモジュール発見の方法には2種類の方法があります．

1. **Pythonのパスが通っているディレクトリにモジュールを保存** : Pythonのパスが通っているディレクトリはPythonターミナル上で ``import sys;print sys.path`` とコマンドを打てば表示できます．大量にディレクトリが表示されると思いますが，その中の一つのディレクトリへ ``/usr/local/lib/python2.7/site-packages/cv2.so`` を移動してください．例えば

    .. code-block:: bash
        
        su mv /usr/local/lib/python2.7/site-packages/cv2.so /usr/lib/python2.7/site-packages
        
残念ながら，OpenCVをインストールする時は必ずこの作業をしなければいけなくなります．

2. ** ``/usr/local/lib/python2.7/site-packages`` ディレクトリを PYTHON_PATH へ追加**: この方法は一度実行すれば十分です． ``~/.bashrc`` を開き，以下のコマンドを追加してください．.bashrcの変更を有効化するために一度ログアウトする必要があります．

    .. code-block:: bash
        
        export PYTHONPATH=$PYTHONPATH:/usr/local/lib/python2.7/site-packages
        
これでOpenCVのインストールは終わりです．Pythonターミナルを開き ``import cv2`` を試してみてください．

ドキュメントは以下のコマンドを実行して作成できます:

    .. code-block:: bash
    
        make docs
        make html_docs
        
``opencv/build/doc/_html/index.html`` をブラウザで開くとドキュメントを見れます．


補足資料
========================

課題
===============        
        
1. FedoraマシンでOpenCVをコンパイルしてみましょう．       
