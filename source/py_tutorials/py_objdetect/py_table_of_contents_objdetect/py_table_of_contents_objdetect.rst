﻿.. _PY_Table-Of-Content-Objdetection:


物体検出
--------------------------------



*  :ref:`face_detection`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |objdet_1|   haar-cascadesを使った顔検出

  =========== ======================================================

  .. |objdet_1|  image:: images/face_icon.jpg
                 :height: 90pt
                 :width:  90pt
                 

               
.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   ../py_face_detection/py_face_detection
