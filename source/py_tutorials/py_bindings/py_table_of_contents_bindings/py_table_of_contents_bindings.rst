﻿.. _PY_Table-Of-Content-Bindings:


OpenCV-Pythonの紐づけ(bindings)
--------------------------------

ここではOpenCV-Pythonの紐づけ(bindings)がどのように生成されるか学びます．


*  :ref:`Bindings_Basics`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |bind1|      OpenCV-Pythonの紐づけ(bindings)の生成方法を学ぶ．

  =========== ======================================================

  .. |bind1|  image:: images/nlm_icon.jpg
                 :height: 90pt
                 :width:  90pt
                 

             
                 
               
.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   ../py_bindings_basics/py_bindings_basics
