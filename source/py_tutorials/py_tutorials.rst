﻿################################
OpenCV-Pythonチュートリアル
################################

*  :ref:`PY_Table-Of-Content-Setup`

   .. tabularcolumns:: m{100pt} m{300pt}
   .. cssclass:: toctableopencv

   =========== =======================================================
   |Introduct| OpenCV-Pythonを使う環境を自分のPC上に構築する方法を学びます

   =========== =======================================================

   .. |Introduct| image:: images/intro.png
                 :height: 80pt
                 :width:  80pt
                 :alt: Introduction Icon

*  :ref:`PY_Table-Of-Content-Gui`

   .. tabularcolumns:: m{100pt} m{300pt}
   .. cssclass:: toctableopencv

   =========== =======================================================
   |Gui|       画像と動画の表示方法と保存方法に加え，GUIの機能であるマウスやトラックバーの作り方を学びます
   =========== =======================================================

   .. |Gui| image:: images/gui.jpg
                 :height: 80pt
                 :width:  80pt
                 :alt: gui Icon

*  :ref:`PY_Table-Of-Content-Core`

   .. tabularcolumns:: m{100pt} m{300pt}
   .. cssclass:: toctableopencv

   =========== =======================================================
   |Core|      画像に対する基本的な処理を学びます．具体的には画素値の編集，幾何変換，コードの最適化(code optimization)，数学関数などです．

   =========== =======================================================

   .. |Core| image:: images/core.jpg
                 :height: 80pt
                 :width:  80pt
                 :alt: core Icon


*  :ref:`PY_Table-Of-Content-ImgProc`

   .. tabularcolumns:: m{100pt} m{300pt}
   .. cssclass:: toctableopencv

   =========== =======================================================
   |ImgProc|   OpenCVが提供する様々な画像処理の関数について学びます．

   =========== =======================================================

   .. |ImgProc| image:: images/imgproc.jpg
                 :height: 80pt
                 :width:  80pt
                 :alt: imgproc Icon

*  :ref:`PY_Table-Of-Content-Feature2D`

   .. tabularcolumns:: m{100pt} m{300pt}
   .. cssclass:: toctableopencv

   =========== =======================================================
   |Feature2D| 特徴検出器，特徴量記述子について学びます．

   =========== =======================================================

   .. |Feature2D| image:: images/featureicon.jpg
                 :height: 80pt
                 :width:  80pt
                 :alt: imgproc Icon


*  :ref:`PY_Table-Of-Content-Video`

   .. tabularcolumns:: m{100pt} m{300pt}
   .. cssclass:: toctableopencv

   =========== =======================================================
   |Video|     動画に対する技術(例えば物体追跡など)を学びます．

   =========== =======================================================

   .. |Video| image:: images/videoicon.jpg
                 :height: 80pt
                 :width:  80pt
                 :alt: imgproc Icon


*  :ref:`PY_Table-Of-Content-Calib`

   .. tabularcolumns:: m{100pt} m{300pt}
   .. cssclass:: toctableopencv

   =========== =======================================================
   |Calib|     カメラキャリブレーション(校正)，ステレオ処理などを学びます．

   =========== =======================================================

   .. |Calib| image:: images/calib3d_icon.jpg
                 :height: 80pt
                 :width:  80pt
                 :alt: Calib Icon



*  :ref:`PY_Table-Of-Content-ML`

   .. tabularcolumns:: m{100pt} m{300pt}
   .. cssclass:: toctableopencv

   =========== =======================================================
   |ML|        OpenCVが提供する機械学習のアルゴリズムの使い方を学びます．

   =========== =======================================================

   .. |ML| image:: images/MachineLearnings.jpg
                 :height: 80pt
                 :width:  80pt
                 :alt: ML Icon


*  :ref:`PY_Table-Of-Content-Photo`

   .. tabularcolumns:: m{100pt} m{300pt}
   .. cssclass:: toctableopencv

   =========== =======================================================
   |Photo|     Computational Photographyの技術(ノイズ除去など)を学びます．

   =========== =======================================================

   .. |Photo| image:: images/photoicon.jpg
                 :height: 80pt
                 :width:  80pt
                 :alt: ML Icon


*  :ref:`PY_Table-Of-Content-Objdetection`

   .. tabularcolumns:: m{100pt} m{300pt}
   .. cssclass:: toctableopencv

   =========== =======================================================
   |Objde|     物体検出(例えば顔検出)について学びます．

   =========== =======================================================

   .. |Objde| image:: images/obj_icon.jpg
                 :height: 80pt
                 :width:  80pt
                 :alt: OD Icon


*  :ref:`PY_Table-Of-Content-Bindings`

   .. tabularcolumns:: m{100pt} m{300pt}
   .. cssclass:: toctableopencv

   =========== =====================================================================
   |PyBin|     OpenCVとPythonをどのように紐付けるか説明します．

   =========== =====================================================================

   .. |PyBin| image:: images/obj_icon.jpg
                 :height: 80pt
                 :width:  80pt
                 :alt: OD Icon
                 
.. raw:: latex

   \pagebreak

.. toctree::
   :maxdepth: 2
   :hidden:
   
   py_setup/py_table_of_contents_setup/py_table_of_contents_setup
   py_gui/py_table_of_contents_gui/py_table_of_contents_gui
   py_core/py_table_of_contents_core/py_table_of_contents_core
   py_imgproc/py_table_of_contents_imgproc/py_table_of_contents_imgproc
   py_feature2d/py_table_of_contents_feature2d/py_table_of_contents_feature2d
   py_video/py_table_of_contents_video/py_table_of_contents_video
   py_calib3d/py_table_of_contents_calib3d/py_table_of_contents_calib3d
   py_ml/py_table_of_contents_ml/py_table_of_contents_ml
   py_photo/py_table_of_contents_photo/py_table_of_contents_photo
   py_objdetect/py_table_of_contents_objdetect/py_table_of_contents_objdetect
   py_bindings/py_table_of_contents_bindings/py_table_of_contents_bindings
