﻿.. _PY_Table-Of-Content-Core:

基本的な処理
-----------------------------------------------------------


*  :ref:`Basic_Ops`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |core_1|    画素値の読み取り方，変更方法，画像の特定領域(ROI)のあ使い方と基本的な処理を学びます．

  =========== ======================================================

  .. |core_1|  image:: images/pixel_ops.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Image_Arithmetics`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |core_2|     画像上での算術演算を学びます．

  =========== ======================================================

  .. |core_2|  image:: images/image_arithmetic.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Optimization_Techniques`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |core_4|     処理をして結果を得ることは重要ですが，最速の方法で結果を得ることはより重要です．ここでは実装したコードの処理速度の確認方法やコードの最適化等について学びます．

  =========== ======================================================

  .. |core_4| image:: images/speed.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Mathematical_Tools`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |core_5|     OpenCVが提供する数学的ツール，主成分分析(PCA)やサポートベクターマシン(SVM)等を学びます．

  =========== ======================================================

  .. |core_5| image:: images/maths_tools.jpg
                 :height: 90pt
                 :width:  90pt

               
.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   ../py_basic_ops/py_basic_ops
   ../py_image_arithmetics/py_image_arithmetics
   ../py_optimization/py_optimization
   ../py_maths_tools/py_maths_tools
