﻿.. _Table-Of-Content-Histograms:

OpenCVでのヒストグラム
-----------------------------------------------------------

*  :ref:`Histograms_Getting_Started`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |hist_1|    ヒストグラムの計算及び描画方法
              
              
  =========== ===================================================================

  .. |hist_1|  image:: images/histograms_1d.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Histogram_Equalization`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |hist_2|    画像のコントラスト改善のためのヒストグラム平坦化
              
              
  =========== ===================================================================

  .. |hist_2|  image:: images/histograms_equ.jpg
                 :height: 90pt
                 :width:  90pt                 

*  :ref:`TwoD_Histogram`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |hist_3|    2次元ヒストグラムの計算及び描画方法
              
              
  =========== ===================================================================

  .. |hist_3|  image:: images/histograms_2d.jpg
                 :height: 90pt
                 :width:  90pt 

*  :ref:`Histogram_Backprojection`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |hist_4|    特定職物体の領域分割のためのヒストグラムのbackprojection
              
              
  =========== ===================================================================

  .. |hist_4|  image:: images/histograms_bp.jpg
                 :height: 90pt
                 :width:  90pt 
                 
.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:
   
   ../py_histogram_begins/py_histogram_begins
   ../py_histogram_equalization/py_histogram_equalization
   ../py_2d_histogram/py_2d_histogram
   ../py_histogram_backprojection/py_histogram_backprojection
