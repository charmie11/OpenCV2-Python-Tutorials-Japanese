.. _PY_Table-Of-Content-ImgProc:

OpenCVを使った画像処理
-----------------------------------------------------------

*  :ref:`Converting_colorspaces`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |imgproc_1| ある画像を別の色空間へ変換する方法を学びます．
              
              また，動画中で特定の色を持つ物体の追跡方法を学びます．
  =========== ===================================================================

  .. |imgproc_1|  image:: images/colorspace.jpg
                 :height: 90pt
                 :width:  90pt
                 
*  :ref:`Geometric_Transformations`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  ============ ===================================================================
  |imgproc_gt| 回転や並進といった幾何変換を画像に適用する方法を学びます．

  ============ ===================================================================

  .. |imgproc_gt|  image:: images/geometric.jpg
                  :height: 90pt
                  :width:  90pt

*  :ref:`Thresholding`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |imgproc_2| 画像の二値化(大局的しきい値，適応的しきい値，大津の二値化)を学びます．

  =========== ===================================================================

  .. |imgproc_2|  image:: images/thresh.jpg
                 :height: 90pt
                 :width:  90pt
                 
*  :ref:`Filtering`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |imgproc_4| 画像の平滑化及び自作した線形フィルタを使った画像のフィルタリング方法を学びます．

  =========== ===================================================================

  .. |imgproc_4|  image:: images/blurring.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Morphological_Ops`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  ============ ===================================================================
  |imgproc_12| モルフォロジー変換(膨張，収縮処理など)を学びます．

  ============ ===================================================================

  .. |imgproc_12|  image:: images/morphology.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Gradients`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |imgproc_5| 画像の勾配，エッジなどの検出方法を学びます．

  =========== ===================================================================

  .. |imgproc_5|  image:: images/gradient.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Canny`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |imgproc_8| Cannyのエッジ検出を学びます．

  =========== ===================================================================

  .. |imgproc_8|  image:: images/canny.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Pyramids`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  ============ ===================================================================
  |imgproc_14| 画像ピラミッドと画像ピラミッドを使ったブレンディングを学びます．

  ============ ===================================================================

  .. |imgproc_14|  image:: images/pyramid.png
                 :height: 90pt
                 :width:  90pt

*  :ref:`Table-Of-Content-Contours`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |imgproc_3| OpenCVが提供する輪郭に関連する様々なものを学びます．

  =========== ===================================================================

  .. |imgproc_3|  image:: images/contours.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Table-Of-Content-Histograms`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |imgproc_6| ヒストグラムについて学びます．

  =========== ===================================================================

  .. |imgproc_6|  image:: images/histogram.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Table-Of-Content-Transforms`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |imgproc_7| フーリエ変換，コサイン変換などの基底を使った画像変換について学びます．

  =========== ===================================================================

  .. |imgproc_7|  image:: images/transforms.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Template_Matching`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |imgproc_9| テンプレートマッチングを使った物体検出を学びます．

  =========== ===================================================================

  .. |imgproc_9|  image:: images/template.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Hough_Lines`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  ============ ===================================================================
  |imgproc_10| 画像中の線の検出方法を学びます．

  ============ ===================================================================

  .. |imgproc_10|  image:: images/houghlines.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Hough_Circles`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  ============ ===================================================================
  |imgproc_11| 画像中の円の検出方法を学びます．

  ============ ===================================================================

  .. |imgproc_11|  image:: images/houghcircles.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Watershed`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  ============ ===================================================================
  |imgproc_13| Watershed algorithmによる画像の領域分割について学びます．

  ============ ===================================================================

  .. |imgproc_13|  image:: images/watershed.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`grabcut`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  ============ ===================================================================
  |imgproc_15| GrabCutを使った前景領域抽出を学びます．

  ============ ===================================================================

  .. |imgproc_15|  image:: images/grabcut.jpg
                 :height: 90pt
                 :width:  90pt


.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   ../py_colorspaces/py_colorspaces
   ../py_thresholding/py_thresholding
   ../py_geometric_transformations/py_geometric_transformations
   ../py_filtering/py_filtering
   ../py_morphological_ops/py_morphological_ops
   ../py_gradients/py_gradients
   ../py_canny/py_canny
   ../py_pyramids/py_pyramids
   ../py_contours/py_table_of_contents_contours/py_table_of_contents_contours
   ../py_histograms/py_table_of_contents_histograms/py_table_of_contents_histograms
   ../py_transforms/py_table_of_contents_transforms/py_table_of_contents_transforms  
   ../py_template_matching/py_template_matching
   ../py_houghlines/py_houghlines
   ../py_houghcircles/py_houghcircles
   ../py_watershed/py_watershed
   ../py_grabcut/py_grabcut
