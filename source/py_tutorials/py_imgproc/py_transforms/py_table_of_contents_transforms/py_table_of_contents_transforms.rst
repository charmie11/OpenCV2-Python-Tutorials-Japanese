﻿.. _Table-Of-Content-Transforms:

OpenCVの基底を使った画像の変換
-----------------------------------------------------------

*  :ref:`Fourier_Transform`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  ============= ===================================================================
  |transform_1| 画像のフーリエ変換
              
              
  ============= ===================================================================

  .. |transform_1|  image:: images/transform_fourier.jpg
                 :height: 90pt
                 :width:  90pt
                 
                 
.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   ../py_fourier_transform/py_fourier_transform
