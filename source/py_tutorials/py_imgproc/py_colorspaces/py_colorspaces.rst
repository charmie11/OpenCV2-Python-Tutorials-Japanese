.. _Converting_colorspaces:

色空間の変換
****************************

目的
=========

    * このチュートリアルでは画像を異なる色空間に変換する方法を学びます．例えばカラー空間からグレースケール空間への変換やBGR空間からHSV空間への変換です．
    * さらに，動画中から特定の色を持つ物体を検出する方法もナビます．
    * 以下の関数の使い方を学びます : **cv2.cvtColor()**, **cv2.inRange()** etc.
    
色変換の変換
======================

OpenCVは150種類以上の色空間の変換を用意していますが，その中で最も広く使われている二つの変換方法(BGR :math:`\leftrightarrow` Gray変換とBGR :math:`\leftrightarrow` HSV変換)を詳しく見ていきます．

色変換に使う関数は ``cv2.cvtColor(input_image, flag)`` です．ここで ``flag`` は色変換の種類を指定するフラグです．

BGR :math:`\rightarrow` Gray変換には ``cv2.COLOR_BGR2GRAY`` フラグ，BGR :math:`\rightarrow` HSV変換には ``cv2.COLOR_BGR2HSV`` フラグを指定します．その他のフラグについて知りたければ，以下のコマンドを実行するとPythonのターミナル上にフラグが列挙されます :
::

    >>> import cv2
    >>> flags = [i for i in dir(cv2) if i.startswith('COLOR_')]
    >>> print flags
 
  
.. note:: HSVの各成分はそれぞれ，Hueが色相，Saturation(Chroma)が彩度，Value(Lightness)が明度を意味します．それぞれuHeは[0,179], Saturationは[0,255]，Valueは[0,255]の範囲の値をとります．使用するソフトウェアによって値の範囲が異なるので，OpenCVで得られた値と別ソフトウェアで得られた値を比較する場合は，値の正規化をしなければいけません．
    
物体追跡
==================

BGR画像をHSV画像へ変換する方法を学んだので，この方法を使って特定の色を持つ物体の検出ができるようになります．HSV空間では，RGB空間より色を表現するのが簡単だからです．以下に示す例では青色をした物体の検出を試みます．処理の流れは以下のようになります:

    * 動画の各フレームを取得
    * 画像をBGR空間からHSV空間へ色変換
    * HSV空間上で，青色に対応する領域をしきい値処理によって見つける
    * 青色をした物体のみを検出できる
    
詳細なコメントをつけたコードをいかに示します． :
::

    import cv2
    import numpy as np

    cap = cv2.VideoCapture(0)

    while(1):
        
        # Take each frame
        _, frame = cap.read()
        
        # Convert BGR to HSV
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        # define range of blue color in HSV
        lower_blue = np.array([110,50,50])
        upper_blue = np.array([130,255,255])
        
        # Threshold the HSV image to get only blue colors
        mask = cv2.inRange(hsv, lower_blue, upper_blue)
        
        # Bitwise-AND mask and original image
        res = cv2.bitwise_and(frame,frame, mask= mask)
        
        cv2.imshow('frame',frame)
        cv2.imshow('mask',mask)
        cv2.imshow('res',res)
        k = cv2.waitKey(5) & 0xFF
        if k == 27:
            break

    cv2.destroyAllWindows()
    
結果を以下に示します．:

     .. image:: images/frame.jpg
              :width: 780 pt  
              :alt: Blue Object Tracking
              :align: center
              
.. note:: 画像中にいくつかノイズが見られます．このノイズの消し方は後のチュートリアルで紹介します．

.. note:: これは物体追跡の最も単純な方法です．輪郭処理に関する関数の使用方法を学べば，物体の重心位置を計算するなどといった様々な処理を行え物体追跡やることになります．

追跡する色(HSV)を調べる方法
-----------------------------------
`stackoverflow.com <www.stackoverflow.com>`_ でよく見る質問の一つがこれです．画像の色変換を行う `cv2.cvtColor()` 関数は特定の色を別の色空間での対応色に変換できます．例えば，緑色に対応するHSVでの色を知りたい場合，いかのようにPythonのターミナル上でコマンドを実行すると情報が得られます
::

    >>> green = np.uint8([[[0,255,0 ]]])
    >>> hsv_green = cv2.cvtColor(green,cv2.COLOR_BGR2HSV)
    >>> print hsv_green
    [[[ 60 255 255]]]
    
下界と上界をそれぞれ [H-10, 100,100]と[H+10, 255, 255] に設定すればいいことが分かります．この方法以外でも，GIMPなどの画像編集ソフトを使えば各色相の値を調べられます．ただし，OpenCVが設定している各色相の範囲に正規化してください．


補足資料
========================

課題
============
#. 同時に赤，青，緑色をしている物体を検出するなど，ある特定の色以外の値を持つ物体を検出してみてください．
