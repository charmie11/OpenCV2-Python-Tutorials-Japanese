﻿.. _Hough_Circles:

ハフ変換による円検出
**************************

目的
=====

このチュートリアルでは
    * ハフ変換による円検出の方法を学びます．
    * 以下の関数の使い方を学びます: **cv2.HoughCircles()**
    
理論
========

円を表す式は :math:`(x-x_{center})^2 + (y - y_{center})^2 = r^2` となります．ここで :math:`(x_{center},y_{center})` は円の中心， :math:`r` は円の半径を表します．円を表すにはこの三つのパラメータを使うので3次元積算機が必要になりますが，これは非効率的です．OpenCVは巧妙な方法 **Hough Gradient Method** を使ってエッジの勾配を使います．

円検出に使う関数は **cv2.HoughCircles()** です．大量の引数がありますが，ドキュメントに詳しく説明されているので，早速コードを見てみましょう．
::

    import cv2
    import numpy as np

    img = cv2.imread('opencv_logo.png',0)
    img = cv2.medianBlur(img,5)
    cimg = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)

    circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,20,
                                param1=50,param2=30,minRadius=0,maxRadius=0)

    circles = np.uint16(np.around(circles))
    for i in circles[0,:]:
        # draw the outer circle
        cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
        # draw the center of the circle  
        cv2.circle(cimg,(i[0],i[1]),2,(0,0,255),3)     

    cv2.imshow('detected circles',cimg)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
結果は以下のようになります:

    .. image:: images/houghcircles2.jpg
        :alt: Hough Circles
        :align: center
        
補足資料
=====================

課題
===========
