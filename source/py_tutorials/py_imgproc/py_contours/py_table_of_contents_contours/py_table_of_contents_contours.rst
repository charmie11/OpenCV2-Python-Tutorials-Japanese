﻿.. _Table-Of-Content-Contours:

OpenCVにおける輪郭(領域)
-----------------------------------------------------------

*  :ref:`Contours_Getting_Started`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |contour_1| 輪郭の検出及び描画方法を学びます．
              
              
  =========== ===================================================================

  .. |contour_1|  image:: images/contour_starting.jpg
                 :height: 90pt
                 :width:  90pt
                 
*  :ref:`Contour_Features`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |contour_2| 輪郭(領域)の様々な特徴(面積，周囲長，外接矩形など)を学びます．

  =========== ===================================================================

  .. |contour_2|  image:: images/contour_features.jpg
                 :height: 90pt
                 :width:  90pt
                 
*  :ref:`Contour_Properties`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |contour_3| 輪郭(領域)の属性情報（Solidityや平均値など）を学びます．

  =========== ===================================================================

  .. |contour_3|  image:: images/contour_properties.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Contours_More_Functions`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |contour_4| 凸性の欠陥（convexity defects）， 多角形近似のテスト，形状のマッチング等について学びます．

  =========== ===================================================================

  .. |contour_4|  image:: images/contour_defects.jpg
                 :height: 90pt
                 :width:  90pt

*  :ref:`Contours_Hierarchy`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ===================================================================
  |contour_5| 輪郭の階層情報について学びます．

  =========== ===================================================================

  .. |contour_5|  image:: images/contour_hierarchy.jpg
                 :height: 90pt
                 :width:  90pt

.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   ../py_contours_begin/py_contours_begin
   ../py_contour_features/py_contour_features
   ../py_contour_properties/py_contour_properties
   ../py_contours_more_functions/py_contours_more_functions
   ../py_contours_hierarchy/py_contours_hierarchy
    

