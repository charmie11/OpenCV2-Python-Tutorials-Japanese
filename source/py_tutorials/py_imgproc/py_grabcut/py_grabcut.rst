﻿.. _grabcut:

GrabCutを使った対話的前景領域抽出
*************************************************************

目的
======

このチュートリアルでは
    * 画像の前景領域を抽出するためのGrabCutアルゴリズムについて学びます．
    * この目的のために対話的アプリケーションを作成します．
    
理論
=========

GrabCutアルゴリズムはイギリスのMicrosoft Research Cambridgeの研究者だったCarsten Rother, Vladimir Kolmogorov, Andrew Blakeらの論文 `"GrabCut": interactive foreground extraction using iterated graph cuts <http://dl.acm.org/citation.cfm?id=1015720>`_ で提案されたアルゴリズムです．使用者の手作業をできるだけ少なくした画像中の前景領域抽出アルゴリズムが必要とされ，GrabCutがその答えになります．

ユーザの観点で見るとどのようなアルゴリズムなのでしょうか? ユーザはまず初めに初期値として前景領域の周りに長方形を描きます(前景物体はこの長方形から飛び出してはいけません)．与えられた初期値を基に，GrabCutは処理を繰り返しながら前景の領域分割を行います．しかし，前景が背景に含まれたり，その逆にといったように分割がうまくいかない時もあります．そのような時は，結果をよくするためにユーザ入力が必要になります．領域分割に失敗した場所に線を描いてください．この線が何を意味するかと言うと， *"背景(前景)に識別されたこの領域は前景(背景)だよ，次の繰り返し計算では修正してね"* とプログラムに伝えるためのユーザ入力になります．ユーザ入力を加えた次の繰り返し計算の結果はより良いものになります．

以下の画像を見てください．初期値として設定する青色の矩形は選手とボールを含んでいます．また，前景を意味する白い線と背景を意味する黒い線がユーザ入力として入力されている点にも気を付けてください．結果として，右側の画像のように良い結果が得られます．

    .. image:: images/grabcut_output1.jpg
        :alt: GrabCut in Action
        :align: center
        
この処理の内部では何が行われているのでしょうか?

    * ユーザが矩形を入力します．この矩形の外側にあるものは全て背景領域であるとみなされます(前景物体は全て矩形内に収まらなければいけないと前述した理由がこれです)．矩形内のものは全てが未知のものであると扱われます．同様に，ユーザ入力によって前景/背景と指定された領域はhard-labellingとして扱われ，以降の繰り返し処理でも値が変わらない領域になります．
    * ユーザが入力したデータを基に初期のラベリングを行い，前景と背景に領域を分割します．
    * 領域分割した前景と背景のモデルを作るために混合正規分布(Gaussian Mixture Model(GMM))を使います．
    * ユーザ入力と画像の画素値を基に，GMMは前景・背景の画素値の分布を学習します．未知の領域として設定された画素に対して，(クラスタリングのように)色の統計値を基に前景領域と背景領域との関係から前景らしさ，背景らしさを計算します．
    * この画素値の分布からグラフを構築します．グラフのノードは画素に **Source** と **Sink** を加えたものになります．全ての前景画素はSourceノードに連結され，全ての背景画素はSinkノードに連結されます．
    * 画素とsource/sinkノードをつなぐエッジの重みは前景らしさ，背景らしさを表す確率によって定義されます．画素間のエッジは画素の類似度や勾配情報によって定義されます．隣接画素間に大きな画素値の差があれば，その画素をつなぐエッジの重みは低くなります．
    * グラフの切断にmincutアルゴリズムを使います．mincutアルゴリズムはコスト関数を最小とするようにグラフをsource側とsink側に切断します．コスト関数は切断されたエッジの重みの合計値です．切断後にsourceノードに連結してある全画素は前景，sinkノードに連結してある全画素は背景領域の画素として認識されます．
    * この前景/背景の識別処理が収束するまで上記の処理を繰り返します．
    
以下の画像が図示しています(画像引用: http://www.cs.ru.ac.za/research/g02m1682/)
    
    .. image:: images/grabcut.jpg
        :alt: Simplified Diagram of GrabCut Algorithm
        :align: center
        
デモ
=======

それではOpenCVを使ったGrabCutアルゴリズムを使ってみましょう．OpenCVは **cv2.grabCut()** という関数を用意しています．引数は以下のようになります:

    * *img* - 入力画像
    * *mask* - マスク画像．背景，前景，背景らしい，前景らしい領域などを指定するためのマスク画像です．フラグとして **cv2.GC_BGD, cv2.GC_FGD, cv2.GC_PR_BGD, cv2.GC_PR_FGD** を指定するか，もしくは0,1,2,3のいずれかの数値を画像に与えます(マスク画像中の画素値が0の画素は背景，1の画素は前景，2の画素は背景らしい，3の画素は前景らしい画素を意味します)．
    * *rect* - 前景物体を囲む矩形領域．フォーマットは(x,y,w,h)で，矩形の左上の位置が(x，y)，サイズが(w，h)です．
    * *bdgModel*, *fgdModel* - これらは内部のアルゴリズムで使用する配列です．サイズが(1,65)のnp.float64型の配列を二つ用意します．
    * *iterCount* - アルゴリズムの繰り返し計算の回数を表します．
    * *mode* - 初期化の方法を指定するフラグです． **cv2.GC_INIT_WITH_RECT** か **cv2.GC_INIT_WITH_MASK** もしくは両方を指定します．それぞれ矩形領域内の情報を基に初期値を決めるか，前景/背景を指定する線を基に初期値を決めるかを表すフラグです．
    
まず初めに **cv2.GC_INIT_WITH_RECT** フラグを指定した結果を見てみましょう．画像を読み込み，似たようなマスク画像を作成します． *fgdModel* と *bgdModel* を作成します．矩形のパラメータを与えます．これらは全て単純です．繰り返し処理の回数は5回に設定します． モードは *cv2.GC_INIT_WITH_RECT* を指定します．設定が全て終わったので，GrabCutを走らせます．結果，マスク画像は更新されます．更新されたマスク画像内では画素は背景と前景を表す4つのどれかに分類されます．画素値が0と2の画素は全て0(つまり背景)に，画素値が1と3の画素は全て1(つまり前景)に変更します．これで最終的なマスク画像ができました．このマスク画像を入力画像に掛け算すれば領域分割された画像が得られます．
::

    import numpy as np
    import cv2
    from matplotlib import pyplot as plt

    img = cv2.imread('messi5.jpg')
    mask = np.zeros(img.shape[:2],np.uint8)

    bgdModel = np.zeros((1,65),np.float64)
    fgdModel = np.zeros((1,65),np.float64)

    rect = (50,50,450,290)
    cv2.grabCut(img,mask,rect,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_RECT)

    mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
    img = img*mask2[:,:,np.newaxis]

    plt.imshow(img),plt.colorbar(),plt.show()
    
結果は以下の容易になります:

    .. image:: images/grabcut_rect.jpg
        :alt: Segmentation in rect mode
        :align: center

おっと，メッシの髪の毛が背景になってしまいました． *髪の毛の無いメッシなんて誰が望むでしょうか?* 髪の毛を取り戻しましょう．前景領域(画素値が1のフラグ)を追加で指定しましょう．同時に前景として識別されているグラウンドやロゴも何とかしましょう．これらの領域に0のフラグ(背景)を指定します．前のデモで得られたマスクを上記のように修正します．

*私が実際何をしたかというと，ペイントツールを使って入力画像を開き，もう一つのレイヤーを追加します．ペイントのブラシツールを使い，未検出の前景領域(髪，靴，ボールなど)を白色にマークし，誤検出してしまった背景領域(ロゴやグラウンドなど)を黒色に設定します．それ以外の領域は灰色に設定します．次に，この作成したマスク画像をOpenCVで読み込み，オリジナルのマスク画像の対応する値を新しいマスク画像の値に置き換えます．以下のコードを見てください:*
::

    # newmask is the mask image I manually labelled
    newmask = cv2.imread('newmask.png',0)

    # whereever it is marked white (sure foreground), change mask=1
    # whereever it is marked black (sure background), change mask=0
    mask[newmask == 0] = 0
    mask[newmask == 255] = 1

    mask, bgdModel, fgdModel = cv2.grabCut(img,mask,None,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_MASK)

    mask = np.where((mask==2)|(mask==0),0,1).astype('uint8')
    img = img*mask[:,:,np.newaxis]
    plt.imshow(img),plt.colorbar(),plt.show()

結果は以下のようになります:

    .. image:: images/grabcut_mask.jpg
        :alt: Segmentation in mask mode
        :align: center
        
以上です．rectモードで初期化する代わりに，直接maskモードから始められます．マスク画像中の矩形領域に2(背景らしい領域)と3(前景らしい領域)のフラグを指定します．次に，二つ目の実装例で行ったように前景領域と確信できる領域をマークします．それからGrabCutアルゴリズムをmaskモードで走らせます．

補足資料
=======================



課題
============

#. OpenCVのサンプルに含まれている ``grabcut.py`` はＧｒａｂＣｕｔを使った対話ツールです．確認してください．使用方法については，この ` youtube video <http://www.youtube.com/watch?v=kAwxLTDDAwU>`_ を見てください．
#. GrabCutアルゴリズムの対話的アプリケーションを作成しましょう．アプリケーションでは，矩形領域と各領域の線をマウスで描き，線の太さをトラックバーで調整できるといった機能をつけてみてください．
