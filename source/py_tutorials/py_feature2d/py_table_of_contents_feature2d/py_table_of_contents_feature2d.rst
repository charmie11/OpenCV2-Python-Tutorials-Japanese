﻿.. _PY_Table-Of-Content-Feature2D:

特徴量検出と特徴量記述
------------------------------------------

*  :ref:`Features_Meaning`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |f2d_1|     画像中の主な特徴とは何か？これらの特徴検出がどのように役立つか？

  =========== ======================================================

  .. |f2d_1|  image:: images/features_icon.jpg
                 :height: 90pt
                 :width:  90pt


*  :ref:`Harris_Corners`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |f2d_2|     コーナーは良い特徴か？どのように検出するのか？

  =========== ======================================================

  .. |f2d_2|  image:: images/harris_icon.jpg
                 :height: 90pt
                 :width:  90pt                 


*  :ref:`shi_tomasi`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |f2d_3|     Shi-Tomasiのコーナー検出について

  =========== ======================================================

  .. |f2d_3|  image:: images/shi_icon.jpg
                 :height: 90pt
                 :width:  90pt 


*  :ref:`sift_intro`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |f2d_4|     Harrisのコーナー検出は画像のスケール変化(拡大・縮小)に対して頑健ではありません．Loweが提案したSIFTと呼ばれるスケール変化に対して不変な特徴について学びます．

  =========== ======================================================

  .. |f2d_4|  image:: images/sift_icon.jpg
                 :height: 90pt
                 :width:  90pt


*  :ref:`SURF`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |f2d_5|     SIFTは精度は良いものの処理速度に問題があります．SIFTの高速版であるSURFについて学びます．

  =========== ======================================================

  .. |f2d_5|  image:: images/surf_icon.jpg
                 :height: 90pt
                 :width:  90pt   


*  :ref:`FAST`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |f2d_06|    上記の特徴点検出手法はSLAMのような実時間アプリケーションで使うには十分なほど高速な処理ではありません．FASTアルゴリズムという文字通り "高速な(FAST)" 計算方法について学びます．

  =========== ======================================================

  .. |f2d_06|  image:: images/fast_icon.jpg
                 :height: 90pt
                 :width:  90pt   


*  :ref:`BRIEF`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |f2d_07|    SIFTは128次元の実ベクトル(浮動小数)を計算します．このような特徴点が数千個もあると想像してください．マッチングの際にメモリ使用量が増大し計算時間がかかってしまいます．高速化のためにSIFT特徴量を圧縮できます．それでも，まず初めにSIFT特徴量を計算しなければいけません．ここではBRIEFという省メモリかつ高速なマッチングが可能な二値ベクトルを計算する特徴量記述子を使います．

  =========== ======================================================

  .. |f2d_07|  image:: images/brief.jpg
                 :height: 90pt
                 :width:  90pt 


*  :ref:`ORB`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |f2d_08|    SIFTとSURFはよく動作しますが，その使用に毎年いくらか支払わなければいけないとしたらどうでしょうか．そうです，SIFTとSURFは特許申請されています．この問題を解決するために，OpenCVはORBというフリーの特徴量記述子を提供しています．
  =========== ======================================================

  .. |f2d_08|  image:: images/orb.jpg
                 :height: 90pt
                 :width:  90pt 


*  :ref:`Matcher`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |f2d_09|    ここまでのチュートリアルで特徴点検出器と特徴量記述子を扱う知識を得ました．次は，特徴量記述子をマッチングする方法を学びましょう．OpenCVは全探索(Brute-Force matcher)と近似最近傍探索(FLANN based matcher)の二つを用意しています．
  =========== ======================================================

  .. |f2d_09|  image:: images/matching.jpg
                 :height: 90pt
                 :width:  90pt 


*  :ref:`feature_homography`

  .. tabularcolumns:: m{100pt} m{300pt}
  .. cssclass:: toctableopencv

  =========== ======================================================
  |f2d_10|    マッチングについても学びました．それでは `calib3d` モジュールと組み合わせて，複雑な画像中から物体を検出してみましょう．
  =========== ======================================================

  .. |f2d_10|  image:: images/homography_icon.jpg
                 :height: 90pt
                 :width:  90pt 

                 
.. raw:: latex

   \pagebreak

.. We use a custom table of content format and as the table of content only informs Sphinx about the hierarchy of the files, no need to show it.
.. toctree::
   :hidden:

   ../py_features_meaning/py_features_meaning
   ../py_features_harris/py_features_harris
   ../py_shi_tomasi/py_shi_tomasi
   ../py_sift_intro/py_sift_intro
   ../py_surf_intro/py_surf_intro
   ../py_fast/py_fast
   ../py_brief/py_brief
   ../py_orb/py_orb
   ../py_matcher/py_matcher
   ../py_feature_homography/py_feature_homography
