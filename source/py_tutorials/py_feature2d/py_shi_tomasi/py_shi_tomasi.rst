﻿.. _shi_tomasi:

Shi-Tomasiのコーナー検出とGood Features to Track(追跡に向いた特徴)
*********************************************************************

目的
=======

このチュートリアルでは,

    * もう一つのコーナー検出法である Shi-Tomasiのコーナー検出を学びます．
    * 以下の関数の使い方を学びます: **cv2.goodFeaturesToTrack()**
    
理論
=========

前チュートリアルではHarrisのコーナー検出について学びました．その後1994年にJ. ShiとC. Tomasiらが **Good Features to Track** という論文で発表した改善案はHarrisのコーナー検出の結果より良い結果を示します．

Harrisのコーナー検出での評価関数は以下のように定義されます:

.. math:: 

    R = \lambda_1 \lambda_2 - k(\lambda_1+\lambda_2)^2
    
この関数の代わりに，Shi-Tomasiのコーナー検出では:

.. math:: 
    
    R = min(\lambda_1, \lambda_2)
    
このRの値が閾値より大きければコーナーとみなします．Harrisのコーナー検出と同様に :math:`\lambda_1 - \lambda_2` 空間でのRの値をプロットすると，以下の図のようになります:

    .. image:: images/shitomasi_space.png
        :alt: Shi-Tomasi Corner Space
        :align: center
        
図より， :math:`\lambda_1` と :math:`\lambda_2` の値が最小値 :math:`\lambda_{min}` 以上の時のみ，コーナー(緑色の領域)とみなされます．

コード
=======

OpenCVでは **cv2.goodFeaturesToTrack()** という関数で実装されています．Shi-Tomasiの手法(指定次第ではHarrisのコーナー検出も使えます)によって画像中のN個のスコアが高いコーナーを検出します．普通はグレースケール画像1枚を入力とし，検出したいコーナーの数を指定します．次に，検出するコーナーの最低限の質を指定する質レベルを0から1の間の値で指定します．最後に検出される2つのコーナー間の最低限のユークリッド距離を与えます．

指定したこれらの全情報を使い，画像中のコーナー検出を行います．スコアが質レベルより低い全コーナーが取り除かれます．閾値処理で残ったコーナーは，スコアを基に降順でソートされます．次に，最もスコアの大きいコーナーの周囲のコーナーを取り除き，最終的にスコアが高いN個のコーナーのみが残ります．

以下の例では25個のコーナーを見つけます:
::

    import numpy as np
    import cv2
    from matplotlib import pyplot as plt

    img = cv2.imread('simple.jpg')
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    corners = cv2.goodFeaturesToTrack(gray,25,0.01,10)
    corners = np.int0(corners)

    for i in corners:
        x,y = i.ravel()
        cv2.circle(img,(x,y),3,255,-1)

    plt.imshow(img),plt.show()
    
See the result below:

    .. image:: images/shitomasi_block1.jpg
        :alt: Shi-Tomasi Corners
        :align: center
        
この関数は物体追跡等により有用です．そのうち，必要な時に説明します．

補足資料
======================


課題
============


