.. _feature_homography:


特徴点のマッチングとHomographyによる物体検出
***********************************************

目的
======

このチュートリアルでは
    * 特徴点のマッチングとcalib3dモジュールのfindHomographyを組み合わせて，複雑な画像中から既知の物体を検出する方法を学びます．
    

基礎
=========

前のチュートリアルでは何をしましたか?クエリ画像上の特徴点を検出し，別の画像上で対応点を検出しました．端的に言うと，もう1枚の画像内にある物体の幾つかの部分の場所を見つけたことになります．この情報は学習画像上の物体の厳密な位置を見つけるのに十分な情報です．

このために，calib3dモジュールの **cv2.findHomography()** という関数を使えます．両方の画像から得られた点の集合を与えると，その物体の射影変換を計算します．次に **cv2.perspectiveTransform()** を使いその物体を検出します．射影変換の計算には最低でも4組の対応点が必要となります．

マッチングの際に，結果に影響を及ぼす可能性がある潜在的なエラーが起こりうることを学んできました．この問題を解決するために，RANSACもしくはLEAST_MEDIANと呼ばれるアルゴリズムを(フラグによって指定して)使います．正しい推定結果を導く良いマッチングはinliersと呼ばれ，それ以外のマッチングはoutliers(外れ値)と呼ばれます． **cv2.findHomography()** はinlierとoutlierの対応点を特定できるマスクを返します．

それでは試してみましょう!!!


コード
=========

まず初めに，いつもどおり，画像中のSIFT特徴量を計算し，ベストなマッチングを見つけるためにratio testを行います．
::

    import numpy as np
    import cv2
    from matplotlib import pyplot as plt

    MIN_MATCH_COUNT = 10

    img1 = cv2.imread('box.png',0)          # queryImage
    img2 = cv2.imread('box_in_scene.png',0) # trainImage

    # Initiate SIFT detector
    sift = cv2.SIFT()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1,None)
    kp2, des2 = sift.detectAndCompute(img2,None)

    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)

    matches = flann.knnMatch(des1,des2,k=2)

    # store all the good matches as per Lowe's ratio test.
    good = []   
    for m,n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)
            
それでは物体を検出するために最低でも10個の対応点対がなければいけないという条件を，MIN_MATCH_COUNTによって設定します．もしくは，単純に十分な対応点対が存在しないとメッセージを表示します．

十分な数の対応点対が見つかれば両画像中の対応が取れた特徴点の座標を取り出します．これらの特徴点は射影変換を計算するために使われます．この3x3の行列が分かればクエリ画像のコーナーを対応する学習画像上の点へと返還できます．それから特徴点を描画します．
::

    if len(good)>MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
        matchesMask = mask.ravel().tolist()

        h,w = img1.shape
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
        dst = cv2.perspectiveTransform(pts,M)

        img2 = cv2.polylines(img2,[np.int32(dst)],True,255,3, cv2.LINE_AA)

    else:
        print "Not enough matches are found - %d/%d" % (len(good),MIN_MATCH_COUNT)
        matchesMask = None
        

最後に，もし物体が検出されればinliersを描画し，そうでなければ対応が取れた特徴点を描画します．
::

    draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                       singlePointColor = None,
                       matchesMask = matchesMask, # draw only inliers
                       flags = 2)  

    img3 = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)

    plt.imshow(img3, 'gray'),plt.show()
    
    

以下に示す結果を見てください．右側に表示されているクエリ画像中の対象物体が白い枠で囲まれていることがわかります:

    .. image:: images/homography_findobj.jpg
        :alt: Finding object with feature homography
        :align: center
        

補足資料
============================


課題
==================        

